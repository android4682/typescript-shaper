import { SpecialMap, StaticImplements } from "@android4682/typescript-toolkit";
import { EBuildProcessResultTypes, IBuildProcess, IBuildProcessResult, IStaticBuildProcess, TBuildProcessRequiredResult, TBuildProcessResults } from "../../src/Domain/Interfaces/BuildProcess";
import { IStaticBuilderArgument } from "../../src/Domain/Interfaces/BuilderArgument";
import { BaseBuildProcessResult } from "../../src/Application/Base/BaseBuildProcessResult";

export class DummyBuildStep implements IBuildProcess, StaticImplements<IStaticBuildProcess, typeof DummyBuildStep>
{
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.VOID
  public static readonly requiredArguments: IStaticBuilderArgument[] = []
  public static readonly optionalArguments: IStaticBuilderArgument[] = []
  public static readonly requiredResults: TBuildProcessRequiredResult[] = []

  public process(stepResults: TBuildProcessResults): any
  {
    return
  }
}

class DummyBuildStep2 extends DummyBuildStep {}
class DummyBuildStep3 extends DummyBuildStep {}
class DummyBuildStep4 extends DummyBuildStep {}
class DummyBuildStep5 extends DummyBuildStep {}

export function getDummyBuildSteps(): IStaticBuildProcess[]
{
  return [DummyBuildStep, DummyBuildStep2, DummyBuildStep3, DummyBuildStep4, DummyBuildStep5]
}

export function generateStepResults(fallbackOnly: boolean = false): TBuildProcessResults
{
  const map = new SpecialMap<string, IBuildProcessResult<any>>()

  if (! fallbackOnly) map.set('DummyFileListName', new BaseBuildProcessResult<string[]>(EBuildProcessResultTypes.FILE_LIST, ['dummyfile1.ts', 'dummyfile2.ts', 'dummyfile3.ts']))
  map.set('DummyFallbackFileListName', new BaseBuildProcessResult<string[]>(EBuildProcessResultTypes.FILE_LIST, ['dummyfallbackfile1.ts', 'dummyfallbackfile2.ts', 'dummyfallbackfile3.ts']))

  return map
}
