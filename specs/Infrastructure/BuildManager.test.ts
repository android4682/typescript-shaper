import { exec as exec_old } from "node:child_process";
import { randomIntRange, randomStringGenerator } from '@android4682/typescript-toolkit';
import path from 'path';
import { IStaticBuilder } from '../../src/Domain/Interfaces/Builder';
import { IBuilderArgument } from '../../src/Domain/Interfaces/BuilderArgument';
import { BuildManager } from '../../src/Infrastructure/BuildManager'
import { shouldRunSuite } from '../SuiteRunChecker';
import { VersionArgument } from "../../src/Application/Arguments/Global/VersionArgument";
import { HelpArgument } from "../../src/Application/Arguments/Global/HelpArgument";
import { ShowOptionsArgument } from "../../src/Application/Arguments/Global/ShowOptionsArgument";
import { ValidBuildersArgument } from "../../src/Application/Arguments/Global/ValidBuildersArgument";
import { BetterConsole } from "@android4682/better-console";

const builderPath = path.join(process.cwd(), 'src', 'Infrastructure', 'index.ts')

const exec = (command: string) => {
  return new Promise((resolve, reject) => exec_old(command, (error, stdout, stderr) => {
    if (error) reject({error, stderr, stdout})
    else resolve(stdout)
  }))
}

class BuildManagerHelper extends BuildManager
{
  public static printFooter(): void
  {
    return super.printFooter()
  }

  public static printApplicationHelpText(text: string | undefined = undefined): void
  {
    return super.printApplicationHelpText(text)
  }

  public static printApplicationOptionsText(text: string | undefined = undefined): void
  {
    return super.printApplicationOptionsText(text)
  }

  public static printApplicationVersion(text: string | undefined = undefined): void
  {
    return super.printApplicationVersion(text)
  }

  public static printValidBuilders(): void
  {
    return super.printValidBuilders()
  }
}

if (shouldRunSuite("UNIT")) {
  BuildManager.exitOnDone = false
  BuildManagerHelper.exitOnDone = false

  describe('The BuildManager', () => {
    it('should have 0 builders when not initiated yet', () => {
      expect(BuildManager.getAllBuilders().size).toBe(0)
    })

    it('should have 0 global arguments when not initiated yet', () => {
      expect(BuildManager.getAllGlobalArgument().size).toBe(0)
    })

    it('should have no application help text when not initiated yet', () => {
      expect(BuildManager.getApplicationHelpText().length).toBe(0)
    })

    it('should have no options text when not initiated yet', () => {
      expect(BuildManager.getApplicationOptionsText().length).toBe(0)
    })

    it('should be able to be initiated', () => {
      expect(BuildManager.initiate()).toBe(true)
    })

    it('won\'t initiated more then once', () => {
      expect(BuildManager.initiate()).toBe(true)
      expect(BuildManager.initiate()).toBe(true)
    })
    
    it('should have some builders when initiated', () => {
      expect(BuildManager.getAllBuilders().size).not.toBe(0)
    })

    it('should have some global arguments when initiated', () => {
      expect(BuildManager.getAllGlobalArgument().size).not.toBe(0)
    })

    it('should have application help text when initiated', () => {
      expect(BuildManager.getApplicationHelpText().length).not.toBe(0)
    })

    it('should have options text when initiated', () => {
      expect(BuildManager.getApplicationOptionsText().length).not.toBe(0)
    })
    
    it('should be able to get a specific builder when initiated', () => {
      const validBuilders = BuildManager.getAllBuilders()
      const validBuilderNames = validBuilders.keysAsArray()
      const randomBuilderName = validBuilderNames[randomIntRange(0, validBuilderNames.length-1)]
      const pickedBuilder = <IStaticBuilder> validBuilders.get(randomBuilderName)
      expect(pickedBuilder).not.toBe(undefined)
      const fetchedBuilder = <IStaticBuilder> BuildManager.getBuilder(randomBuilderName)
      expect(fetchedBuilder).not.toBe(undefined)
      expect(fetchedBuilder).toBe(pickedBuilder)
    })

    it('should be able to get a specific builder global arguments when initiated', () => {
      const validGlobalArguments = BuildManager.getAllGlobalArgument()
      const validGlobalArgumentNames = validGlobalArguments.keysAsArray()
      const randomGlobalArgumentName = validGlobalArgumentNames[randomIntRange(0, validGlobalArgumentNames.length-1)]
      const pickedGlobalArgument = <IBuilderArgument> validGlobalArguments.get(randomGlobalArgumentName)
      expect(pickedGlobalArgument).not.toBe(undefined)
      const fetchedGlobalArgument = <IBuilderArgument> BuildManager.getGlobalArgument(randomGlobalArgumentName)
      expect(fetchedGlobalArgument).not.toBe(undefined)
      expect(fetchedGlobalArgument).toBe(pickedGlobalArgument)
    })

    it('should be able to print a help text with a random value (via Helper)', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'log')
      const value = randomStringGenerator(32)
      BuildManagerHelper.printApplicationHelpText(value)
      expect(spy).toHaveBeenCalledWith(value)
    })

    it('should be able to print a help text (via Helper)', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'log')
      BuildManagerHelper.printApplicationHelpText()
      expect(spy).toHaveBeenCalled()
    })

    it('should be able to print a option text (via Helper)', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'log')
      const value = randomStringGenerator(32)
      BuildManagerHelper.printApplicationOptionsText(value)
      expect(spy).toHaveBeenCalledWith(value)
    })

    it('should be able to print a option text with a random value (via Helper)', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'log')
      BuildManagerHelper.printApplicationOptionsText()
      expect(spy).toHaveBeenCalled()
    })

    it('should be able to print a version text (via Helper)', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'log')
      const value = randomStringGenerator(32)
      BuildManagerHelper.printApplicationVersion(value)
      expect(spy).toHaveBeenCalledWith(value)
    })

    it('should be able to print a version text a random value (via Helper)', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'log')
      BuildManagerHelper.printApplicationVersion()
      expect(spy).toHaveBeenCalled()
    })

    it('should be able to print a list of valid builders (via Helper)', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'info')
      BuildManagerHelper.printValidBuilders()
      expect(spy).toHaveBeenCalled()
    })

    it('should return a message about the usage of this program when the main process has been run', () => {
      const spy = jest.spyOn(BetterConsole.prototype, 'error')
      BuildManagerHelper.main()
      expect(spy).toHaveBeenCalledWith(`No builder has been set/found and no global arguments have been passed. Try '${HelpArgument.argumentSelector.join("' or '")}' to see the help text.`)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  describe('The BuildManager', () => {
    it('should print an error when no builder or global argument has been set', () => {
      jest.setTimeout(10000)
      return new Promise<void>(async (resolve, reject) => {
        let cmd = `npx ts-node ${builderPath}`
        console.debug(cmd)
        exec(cmd).then((value) => {
          const output = <string> value
          console.debug(output)
          expect(output.includes('No builder has been set/found and no global arguments have been passed.')).toBe(true)
          resolve()
        }).catch((err) => {
          console.debug(err)
          const error = <Error> err.error
          expect(error.message.includes('No builder has been set/found and no global arguments have been passed.')).toBe(true)
          resolve()
        })
      })
    })
  
    it('should print a version number when correct argument has been passed', () => {
      jest.setTimeout(10000)
      return new Promise<void>(async (resolve, reject) => {
        let cmd = `npx ts-node ${builderPath} ${VersionArgument.argumentSelector[0]}`
        console.debug(cmd)
        exec(cmd).then((value) => {
          console.debug(value)
          const output = <string> value
          expect(new RegExp(/^\d*\.\d*\.\d*/).exec(output)).not.toBe(undefined)
          resolve()
        }).catch((err) => {
          reject(err)
        })
      })
    })
  
    it('should print help text when correct argument has been passed', () => {
      jest.setTimeout(10000)
      return new Promise<void>(async (resolve, reject) => {
        let cmd = `npx ts-node ${builderPath} ${HelpArgument.argumentSelector[0]}`
        console.debug(cmd)
        exec(cmd).then((value) => {
          console.debug(value)
          const output = <string> value
          expect(output.includes('Shows help information.')).toBe(true)
          resolve()
        }).catch((err) => {
          reject(err)
        })
      })
    })
  
    it('should print options when correct argument has been passed', () => {
      jest.setTimeout(10000)
      return new Promise<void>(async (resolve, reject) => {
        let cmd = `npx ts-node ${builderPath} ${ShowOptionsArgument.argumentSelector[0]}`
        console.debug(cmd)
        exec(cmd).then((value) => {
          console.debug(value)
          const output = <string> value
          expect(new RegExp(/ = /, 'gm').exec(output)).not.toBe(undefined)
          resolve()
        }).catch((err) => {
          reject(err)
        })
      })
    })
  
    it('should print valid builders when correct argument has been passed', () => {
      jest.setTimeout(10000)
      return new Promise<void>(async (resolve, reject) => {
        let cmd = `npx ts-node ${builderPath} ${ValidBuildersArgument.argumentSelector[0]}`
        console.debug(cmd)
        exec(cmd).then((value) => {
          console.debug(value)
          const output = <string> value
          expect(output.includes('Valid builders:')).toBe(true)
          resolve()
        }).catch((err) => {
          reject(err)
        })
      })
    })
  
    it('should print valid builders when correct argument has been passed', () => {
      jest.setTimeout(10000)
      return new Promise<void>(async (resolve, reject) => {
        let cmd = `npx ts-node ${builderPath} ${ValidBuildersArgument.argumentSelector[0]}`
        console.debug(cmd)
        exec(cmd).then((value) => {
          console.debug(value)
          const output = <string> value
          expect(output.includes('Valid builders:')).toBe(true)
          resolve()
        }).catch((err) => {
          reject(err)
        })
      })
    })
  })
}