import { SingleFileBuilder } from '../../../src/Infrastructure/Builders/SingleFileBuilder'
import { SourceFolderArgument } from "../../../src/Application/Arguments/Builder/SourceFolderArgument"
import { DistFolderArgument } from "../../../src/Application/Arguments/Builder/DistFolderArgument"
import { DistFileNameArgument } from "../../../src/Application/Arguments/Builder/DistFileNameArgument"
import { BuildPathArgument } from "../../../src/Application/Arguments/Builder/BuildPathArgument"
import { EnableTypeBuildingArgument } from "../../../src/Application/Arguments/Builder/EnableTypeBuildingArgument"
import { EnableSourceMapBuildingArgument } from "../../../src/Application/Arguments/Builder/EnableSourceMapBuildingArgument"
import { shouldRunSuite } from '../../SuiteRunChecker';
import fs from "node:fs";
import { exec as exec_old } from "node:child_process";
import path from "node:path";

const exec = (command: string) => {
  return new Promise((resolve, reject) => exec_old(command, (error, stdout, stderr) => {
    if (error) reject({error, stderr, stdout})
    else resolve(stdout)
  }))
}

function deleteFile(filePath: string): void
{
  if (fs.lstatSync(filePath).isDirectory()) {
    const files = fs.readdirSync(filePath)
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const nFilePath = path.join(filePath, file)
      if (fs.lstatSync(nFilePath).isDirectory()) deleteFile(nFilePath)
      else if (fs.existsSync(nFilePath)) fs.rmSync(nFilePath)
    }
    if (fs.existsSync(filePath)) fs.rmdirSync(filePath)
  } else {
    if (fs.existsSync(filePath)) fs.rmSync(filePath)
  }
}

const builderPath = path.join(process.cwd(), 'src', 'Infrastructure', 'index.ts')
const specTestPath = path.join(process.cwd(), 'specs', 'tests')
const thisSpecTestPath = path.join(specTestPath, 'SingleFileBuilder')
const buildPath = path.join(thisSpecTestPath, 'build')
const specTestSrcPath = path.join(thisSpecTestPath, 'src')
const specTestDistPath = path.join(thisSpecTestPath, 'dist')
const specTestResultFilePath = path.join(specTestDistPath, 'finished.js')

const correctSpecBuildOrder = [path.join(specTestSrcPath, 'Main.ts'), path.join(specTestSrcPath, 'index.ts')]

if (fs.existsSync(specTestDistPath)) deleteFile(specTestDistPath)
if (fs.existsSync(buildPath)) deleteFile(buildPath)

if (shouldRunSuite('UNIT')) {
  describe('The SingleFileBuilder', () => {
    it('can be created by a helper', () => {
      const builder = new SingleFileBuilder()
  
      expect(builder instanceof SingleFileBuilder).toBe(true)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  jest.setTimeout(20000)

  describe('The SingleFileBuilder', () => {
    it('can build the test files', () => {
      return new Promise<void>((resolve, reject) => {
        if (fs.existsSync(specTestDistPath)) deleteFile(specTestDistPath)
        fs.writeFileSync(path.join(specTestSrcPath, 'build_order.json'), JSON.stringify(correctSpecBuildOrder))

        jest.spyOn(SourceFolderArgument.prototype, 'value', 'get').mockReturnValue(specTestSrcPath)
        jest.spyOn(DistFolderArgument.prototype, 'value', 'get').mockReturnValue(specTestDistPath)
        jest.spyOn(DistFileNameArgument.prototype, 'value', 'get').mockReturnValue("finished.js")
        jest.spyOn(BuildPathArgument.prototype, 'value', 'get').mockReturnValue(buildPath)
        jest.spyOn(EnableTypeBuildingArgument.prototype, 'value', 'get').mockReturnValue(true)
        jest.spyOn(EnableSourceMapBuildingArgument.prototype, 'value', 'get').mockReturnValue(true)

        new SingleFileBuilder().runBuild().then(() => {
          expect(fs.existsSync(specTestResultFilePath)).toBe(true)
          expect(fs.existsSync(path.join(specTestDistPath, 'finished.d.ts'))).toBe(true)
          expect(fs.existsSync(path.join(specTestDistPath, 'finished.js.map'))).toBe(true)
          resolve()
        }).catch((err) => {
          console.error(err)
          expect(fs.existsSync(specTestResultFilePath)).toBe(true)
          expect(fs.existsSync(path.join(specTestDistPath, 'finished.d.ts'))).toBe(true)
          expect(fs.existsSync(path.join(specTestDistPath, 'finished.js.map'))).toBe(true)
          reject(err)
        })
      })
    })
  })

  describe('The SingleFileBuilder\'s spec test', () => {
    it('should return a non-zero on execute (and write a funny joke)', () => {
      return new Promise<void>((resolve, reject) => {
        exec(`node ${specTestResultFilePath}`).then((output) => {
          let strOutput = <string> output
          console.debug(output)
          expect(strOutput.startsWith('I predict this test will')).toBe(true);
          resolve()
        }).catch((err) => {
          expect(false).toBe(true);
          reject(err)
        })
      })
    })
  })
}
