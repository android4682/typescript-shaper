import { IStaticBuilder } from '../../../src/Domain/Interfaces/Builder';
import { GenerateBuildFileOrderBuilder } from '../../../src/Infrastructure/Builders/GenerateBuildFileOrderBuilder'
import { shouldRunSuite } from '../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The Static Generate Builder File Order Builder', () => {
    it('should have a name', () => {
      expect(GenerateBuildFileOrderBuilder.builder_name).not.toBe(undefined)
    })
    
    it('should have a different name then the parent builder', () => {
      const parentBuilder = <IStaticBuilder> Object.getPrototypeOf(GenerateBuildFileOrderBuilder)
      
      if (parentBuilder.name !== '') {
        expect(GenerateBuildFileOrderBuilder.builder_name).not.toBe(parentBuilder.builder_name)
      } else {
        console.warn(`${GenerateBuildFileOrderBuilder.name} doesn't have a parent builder!`)
      }
    })
    
    it('should have some build steps', () => {
      let buildSteps = 0
        + GenerateBuildFileOrderBuilder.pre_build_steps.getAllSteps().length
        + GenerateBuildFileOrderBuilder.build_steps.getAllSteps().length
        + GenerateBuildFileOrderBuilder.compiler_steps.getAllSteps().length
        + GenerateBuildFileOrderBuilder.post_build_steps.getAllSteps().length

      expect(buildSteps).toBeGreaterThan(0)
    })
  })


  describe('The Generate Builder File Order Builder', () => {
    it('can be initiated', () => {
      const builder = new GenerateBuildFileOrderBuilder()
    
      expect(builder instanceof GenerateBuildFileOrderBuilder).toBe(true)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Generate Builder File Order Builder Suite', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}