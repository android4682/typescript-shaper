import { MissingArgumentsException } from '../../../src/Application/Exceptions/MissingArgumentsException';
import { shouldRunSuite } from '../../SuiteRunChecker';
import { randomStringGenerator } from '@android4682/typescript-toolkit';

if (shouldRunSuite('UNIT')) {
  describe('The Missing Arguments Exception', () => {
    it('can hold a message', () => {
      const message = randomStringGenerator(32)

      expect(new MissingArgumentsException(message).message).toBe(message)
    })

    it('can generate itself with an argument', () => {
      const argument = randomStringGenerator(32)
      
      expect(MissingArgumentsException.createDefault(argument).message).toBe(`Missing ${argument} argument.`)
    })
    
    it('can generate itself without an argument', () => {
      expect(MissingArgumentsException.createDefault().message).toBe(`Missing argument.`)
    })
    
    it('can generate from a build process', () => {
      const processName = randomStringGenerator(32)
      const argumentName = randomStringGenerator(32)

      expect(MissingArgumentsException.fromBuildProcess(processName, argumentName).message).toBe(`Missing builder argument '${argumentName}' for '${processName}'.`)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Missing Arguments Exception', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
