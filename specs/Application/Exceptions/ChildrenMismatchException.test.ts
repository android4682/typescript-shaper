import { ChildrenMismatchException } from '../../../src/Application/Exceptions/ChildrenMismatchException';
import { shouldRunSuite } from '../../SuiteRunChecker';
import { randomStringGenerator } from '@android4682/typescript-toolkit';

if (shouldRunSuite('UNIT')) {
  describe('The Children Mismatch Exception', () => {
    it('can hold a message', () => {
      const message = randomStringGenerator(32)

      expect(new ChildrenMismatchException(message).message).toBe(message)
    })

    it('can generate itself', () => {
      const inPlace = randomStringGenerator(32)
      const element = randomStringGenerator(32)
      
      expect(ChildrenMismatchException.default(inPlace, element).message).toBe(`Children '${element}' mismatch in '${inPlace}'.`)
    })

    it('can generate itself without an element', () => {
      const inPlace = randomStringGenerator(32)
      
      expect(ChildrenMismatchException.default(inPlace).message).toBe(`Children 'length' mismatch in '${inPlace}'.`)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Children Mismatch Exception', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
