import { BaseHelpBuildArgumentModule } from '../../../src/Application/Base/BaseHelpModule';
import { BaseBuildArgument } from '../../../src/Application/Base/BaseBuildArgument';
import { shouldRunSuite } from '../../SuiteRunChecker';
import { randomStringGenerator } from '@android4682/typescript-toolkit';

if (shouldRunSuite('UNIT')) {
  describe('The Base Help Build Argument Module', () => {
    it('can be created', () => {
      const helpBuildArgument = new BaseHelpBuildArgumentModule(BaseBuildArgument)
      expect(helpBuildArgument).not.toBe(undefined)
    })

    it('should have the past build argument', () => {
      const helpBuildArgument = new BaseHelpBuildArgumentModule(BaseBuildArgument)
      expect(helpBuildArgument.buildArgument).toBeInstanceOf(BaseBuildArgument)
    })
    
    it('should not be required', () => {
      const helpBuildArgument = new BaseHelpBuildArgumentModule(BaseBuildArgument)
      expect(helpBuildArgument.isRequired).toBe(false)
    })
    
    it('should not be used by anything', () => {
      const helpBuildArgument = new BaseHelpBuildArgumentModule(BaseBuildArgument)
      let process = randomStringGenerator(32)
      
      expect(helpBuildArgument.isUsedBy(process)).toBe(false)
    })
    
    it('should be able to add required by', () => {
      const helpBuildArgument = new BaseHelpBuildArgumentModule(BaseBuildArgument)
      let process = randomStringGenerator(32)

      expect(helpBuildArgument.isRequired).toBe(false)
      helpBuildArgument.addRequiredBy(process)
      helpBuildArgument.addRequiredBy(process)
      expect(helpBuildArgument.isRequired).toBe(true)
    })
    
    it('should be able to add used by by', () => {
      const helpBuildArgument = new BaseHelpBuildArgumentModule(BaseBuildArgument)
      let process = randomStringGenerator(32)

      expect(helpBuildArgument.isUsedBy(process)).toBe(false)
      helpBuildArgument.addUsedBy(process)
      helpBuildArgument.addUsedBy(process)
      expect(helpBuildArgument.isUsedBy(process)).toBe(true)
    })
    
    it('should throw an error when running fromBuildArgument if a build argument is passed that does not have a argument selector', () => {
      expect(() => BaseHelpBuildArgumentModule.fromBuildArgument.call(BaseHelpBuildArgumentModule, BaseBuildArgument)).toThrowError()
    })
    
    it('should throw an error when running fromBatchBuildArgument if a build argument is passed that does not have a argument selector', () => {
      expect(() => BaseHelpBuildArgumentModule.fromBatchBuildArgument.call(BaseHelpBuildArgumentModule, [new BaseBuildArgument()])).toThrowError()
    })
    
    it('should not throw an error when running fromBatchBuildArgument if a build argument is passed that does not have a argument selector and should ignore missing argument selectors', () => {
      expect(() => BaseHelpBuildArgumentModule.fromBatchBuildArgument.call(BaseHelpBuildArgumentModule, [new BaseBuildArgument()], true)).not.toThrowError()
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Base Help Build Argument Module', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
