import { BaseBuildProcess } from '../../../src/Application/Base/BaseBuildProcess';
import { shouldRunSuite } from '../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The Base Build Process', () => {
    it('can be created', () => {
      expect(new BaseBuildProcess()).not.toBe(undefined)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Base Build Process', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
