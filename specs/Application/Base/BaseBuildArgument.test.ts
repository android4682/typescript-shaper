import { BaseBuildArgument } from '../../../src/Application/Base/BaseBuildArgument';
import { shouldRunSuite } from '../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The Base Build Argument', () => {
    it('can be created', () => {
      expect(new BaseBuildArgument()).not.toBe(undefined)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Base Build Argument', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
