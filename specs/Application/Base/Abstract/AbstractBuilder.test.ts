import { isDeepInstanceOf, randomIntRange, SpecialMap } from "@android4682/typescript-toolkit"
import { AbstractBuilder } from '../../../../src/Application/Base/Abstract/AbstractBuilder'
import { shouldRunSuite } from "../../../SuiteRunChecker";

class AbstractBuilderHelper extends AbstractBuilder {
  public shouldRunStep(currentStep: number, input: number | number[] | undefined): boolean
  {
    return super.shouldRunStep(currentStep, input)
  }
}

if (shouldRunSuite("UNIT")) {
  describe('The Static Abstract Builder', () => {
    it('should have a name', () => {
      expect(AbstractBuilder.builder_name).not.toBe(undefined)
    })
    
    it('should have 0 help build arguments as a SpecialMap', () => {
      const map = AbstractBuilder.help_build_arguments
  
      expect(isDeepInstanceOf(map.constructor, SpecialMap)).toBe(true)
      expect(map.size).toBe(0)
    })
    
    it('should have 0 build arguments', () => {
      const buildArgument = AbstractBuilder.getBuildArguments()
      expect(buildArgument.length).toBe(0)
    })
    
    it('should have a help text and that should be a string', () => {
      let helpText = AbstractBuilder.getHelpText()
      expect(helpText).not.toBe(undefined)
      expect(typeof helpText).toBe('string')
    })
    
    it('should have an empty options text and that should be a string', () => {
      let optionsText = AbstractBuilder.getOptionsText()
      expect(optionsText).not.toBe(undefined)
      expect(typeof optionsText).toBe('string')
      expect(optionsText.length).toBe(0)
    })
  })
  
  describe('The Abstract Builder', () => {
    it('can be initiated via a helper', () => {
      const builder = new AbstractBuilderHelper()
  
      expect(builder instanceof AbstractBuilderHelper).toBe(true)
    })
  
    it('should run any build step if not limits are passed', () => {
      const builder = new AbstractBuilderHelper()
  
      expect(builder.shouldRunStep(randomIntRange(0, 10), undefined)).toBe(true)
    })
  
    it('should run build step if limit array include given step', () => {
      const builder = new AbstractBuilderHelper()
      const step = randomIntRange(0, 10)
      const runOnly: number[] = [step, randomIntRange(0, 10), randomIntRange(0, 10), randomIntRange(0, 10)]
  
      expect(builder.shouldRunStep(step, runOnly)).toBe(true)
    })
  
    it('should not run build step if limit array doesn\'t include given step', () => {
      const builder = new AbstractBuilderHelper()
      const step = 11
      const runOnly: number[] = [randomIntRange(0, 10), randomIntRange(0, 10), randomIntRange(0, 10)]
  
      expect(builder.shouldRunStep(step, runOnly)).toBe(false)
    })
  
    it('should run build step if limit is the given step', () => {
      const builder = new AbstractBuilderHelper()
      const step = randomIntRange(0, 10)
  
      expect(builder.shouldRunStep(step, step)).toBe(true)
    })
  
    it('should not run build step if limit is not the given step', () => {
      const builder = new AbstractBuilderHelper()
      const step = randomIntRange(0, 10)
      const runOnly = 99
  
      expect(builder.shouldRunStep(step, runOnly)).toBe(false)
    })
  
    it('can run build without an error', () => {
      return new Promise<void>(async (resolve, reject) => {
        const builder = new AbstractBuilderHelper()
    
        try {
          expect(await builder.runBuild()).toBe(undefined)
          resolve()
        } catch(e) {
          expect(e).toBe(undefined)
          reject()
        }
      })
    })
  })
}

if (shouldRunSuite("FUNCTIONAL")) {
  if (! shouldRunSuite("UNIT")) {
    describe('The Abstract Builder Suite', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}