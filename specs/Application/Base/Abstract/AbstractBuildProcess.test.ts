import { shouldRunSuite } from "../../../SuiteRunChecker";
import { AbstractBuildProcess } from "../../../../src/Application/Base/Abstract/AbstractBuildProcess";
import { generateStepResults } from "../../../Helper/BuildStep";

class AbstractBuildProcessHelper extends AbstractBuildProcess {
}

if (shouldRunSuite("UNIT")) {
  describe('The Abstract Build Process (via helper)', () => {
    it('should be able to be initiated', () => {
      let buildProcess = undefined
      try {
        buildProcess = new AbstractBuildProcessHelper()
      } catch (e) {
      }
      expect(buildProcess).not.toBe(undefined)
    })

    it('should be able to process step results', () => {
      let buildProcess = new AbstractBuildProcessHelper()

      expect(buildProcess.process(generateStepResults())).toBe(undefined)
    })
  })
}

if (shouldRunSuite("FUNCTIONAL")) {
  if (! shouldRunSuite("UNIT")) {
    describe('The Abstract Builder Suite', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}