import { randomStringGenerator } from "@android4682/typescript-toolkit"
import { shouldRunSuite } from "../../../SuiteRunChecker";
import { AbstractBuildArgument } from "../../../../src/Application/Base/Abstract/AbstractBuildArgument";

class AbstractBuildArgumentHelper extends AbstractBuildArgument {
}

if (shouldRunSuite("UNIT")) {
  describe('The Abstract Build Argument (via helper)', () => {
    it('should be able to be initiated', () => {
      let buildArguement = undefined
      try {
        buildArguement = new AbstractBuildArgumentHelper()
      } catch (e) {
      }
      expect(buildArguement).not.toBe(undefined)
    })

    it('should be able to override the value', () => {
      let defaultValue = randomStringGenerator(64)
      let buildArguement = new AbstractBuildArgumentHelper(defaultValue)
      expect(buildArguement.value).toBe(defaultValue)

      let newValue = randomStringGenerator(64)
      buildArguement.value = newValue
      
      expect(buildArguement.value).toBe(newValue)
    })

    it('should be able to process an arguement', () => {
      let defaultValue = randomStringGenerator(64)
      let buildArguement = new AbstractBuildArgumentHelper(defaultValue)

      let processedValue = buildArguement.process()

      expect(processedValue).toBe(defaultValue)
    })
  })
}

if (shouldRunSuite("FUNCTIONAL")) {
  if (! shouldRunSuite("UNIT")) {
    describe('The Abstract Builder Suite', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}