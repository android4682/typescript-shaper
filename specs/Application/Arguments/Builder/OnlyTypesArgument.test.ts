import { OnlyTypesArgument } from '../../../../src/Application/Arguments/Builder/OnlyTypesArgument';
import { shouldRunSuite } from '../../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The OnlyTypesArgument', () => {
    it('can be created', () => {
      expect(new OnlyTypesArgument()).not.toBe(undefined)
    })

    it('should have an argument selector', () => {
      expect(OnlyTypesArgument.argumentSelector.length).toBeGreaterThan(0)
    })

    it('should have a help description', () => {
      expect(OnlyTypesArgument.helpDescription.length).toBeGreaterThan(0)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The OnlyTypesArgument', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
