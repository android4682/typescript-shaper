import { OnlyJoinArgument } from '../../../../src/Application/Arguments/Builder/OnlyJoinArgument';
import { shouldRunSuite } from '../../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The OnlyJoinArgument', () => {
    it('can be created', () => {
      expect(new OnlyJoinArgument()).not.toBe(undefined)
    })

    it('should have an argument selector', () => {
      expect(OnlyJoinArgument.argumentSelector.length).toBeGreaterThan(0)
    })

    it('should have a help description', () => {
      expect(OnlyJoinArgument.helpDescription.length).toBeGreaterThan(0)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The OnlyJoinArgument', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
