import { AddCwdToSrcPathArgument } from '../../../../src/Application/Arguments/Builder/AddCwdToSrcPathArgument';
import { shouldRunSuite } from '../../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The Add Cwd To Src Path Argument', () => {
    it('can be created', () => {
      expect(new AddCwdToSrcPathArgument()).not.toBe(undefined)
    })

    it('should have an argument selector', () => {
      expect(AddCwdToSrcPathArgument.argumentSelector.length).toBeGreaterThan(0)
    })

    it('should have a help description', () => {
      expect(AddCwdToSrcPathArgument.helpDescription.length).toBeGreaterThan(0)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Add Cwd To Src Path Argument', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
