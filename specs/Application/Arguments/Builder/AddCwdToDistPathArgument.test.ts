import { AddCwdToDistPathArgument } from '../../../../src/Application/Arguments/Builder/AddCwdToDistPathArgument';
import { shouldRunSuite } from '../../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The Add Cwd To Dist Path Argument', () => {
    it('can be created', () => {
      expect(new AddCwdToDistPathArgument()).not.toBe(undefined)
    })

    it('should have an argument selector', () => {
      expect(AddCwdToDistPathArgument.argumentSelector.length).toBeGreaterThan(0)
    })

    it('should have a help description', () => {
      expect(AddCwdToDistPathArgument.helpDescription.length).toBeGreaterThan(0)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Add Cwd To Dist Path Argument', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
