import { MatrixManager } from '../../../src/Application/Classes/MatrixManager'
import { randomStringGenerator, randomIntRange } from '@android4682/typescript-toolkit';
import { shouldRunSuite } from '../../SuiteRunChecker';

if (shouldRunSuite('UNIT')) {
  describe('The Matrix Manager', () => {
    it('should return an empty row when just created', () => {
      const mm = new MatrixManager()
      
      const row = mm.getRow(0)

      expect(row.length).toBe(0)
      expect(Array.isArray(row)).toBe(true)
    })

    it("'s next empty row index should be 0", () => {
      const mm = new MatrixManager()

      expect(mm.getNextEmptyRowIndex()).toBe(0)
    })

    it("'s next empty row should be empty upon getting", () => {
      const mm = new MatrixManager()
      
      const nextEmptyRow = mm.getNextEmptyRow()

      expect(nextEmptyRow.length).toBe(0)
      expect(Array.isArray(nextEmptyRow)).toBe(true)
    })

    it("fetching next empty row should created a new row.", () => {
      const mm = new MatrixManager()
      
      mm.getNextEmptyRow()
      const row = mm.getRow(0)

      expect(row.length).toBe(0)
      expect(Array.isArray(row)).toBe(true)
    })
    
    it("fetching a random column row should return undefined when just created", () => {
      const mm = new MatrixManager()
      
      const value = mm.getColumnRow(0, 0)
      const row = mm.getRow(0)
      
      expect(value).toBe(undefined)
      expect(row.length).toBe(0)
      expect(Array.isArray(row)).toBe(true)
    })
    
    it("should have value that this test will set in the next empty row at column index 0", () => {
      const mm = new MatrixManager<string>()
      
      const value = randomStringGenerator(16)
      mm.addColumnToNextEmptyRow(0, value)

      expect(mm.getColumnRow(0, 0)).toBe(value)

      const row = mm.getRow(0)
      expect(row.length).toBe(1)
      expect(Array.isArray(row)).toBe(true)
      expect(row[0]).toBe(value)
    })
    
    it("should have value that this test will set at a random index", () => {
      const mm = new MatrixManager<string>()
      
      const rowIndex = randomIntRange(0, 99)
      const columnIndex = randomIntRange(0, 99)
      const value = randomStringGenerator(16)

      mm.addColumnRow(rowIndex, columnIndex, value)

      expect(mm.getColumnRow(rowIndex, columnIndex)).toBe(value)

      const row = mm.getRow(rowIndex)
      expect(row.length).toBe(columnIndex+1)
      expect(Array.isArray(row)).toBe(true)
      expect(row[columnIndex]).toBe(value)
    })
    
    it("should break forEachRow when callback function returns false", () => {
      const mm = new MatrixManager<string>()
      
      const rowIndex = randomIntRange(0, 99)
      const columnIndex = randomIntRange(0, 99)
      const value = randomStringGenerator(16)

      mm.addColumnRow(rowIndex, columnIndex, value)

      mm.forEachRow((row, _rowIndex) => {
        expect(row.length).toBe(columnIndex+1)
        expect(_rowIndex).toBe(rowIndex)
        return false
      })
    })
    
    it("should break forEachColumn when callback function returns false", () => {
      const mm = new MatrixManager<string>()
      
      const rowIndex = randomIntRange(0, 99)
      const columnIndex = randomIntRange(0, 99)
      const value = randomStringGenerator(16)

      mm.addColumnRow(rowIndex, columnIndex, value)

      mm.forEachColumn((value, _columnIndex, _rowIndex) => {
        expect(value).toBe(value)
        expect(_columnIndex).toBe(columnIndex)
        expect(_rowIndex).toBe(rowIndex)
        return false
      })
    })
    
    it("should be able to handle a bunch of random data at random indexes", () => {
      const mm = new MatrixManager<string>()
      // rowIndex, columnIndex, value, covered
      const data: [number, number, string, boolean][] = []
      const findInData = (dataArr: [number, number, string, boolean][], value: string): number => {
        for (let i = 0; i < dataArr.length; i++) {
          const dataEntry = dataArr[i];
          if (dataEntry[2] === value) return i
        }

        return -1
      } 
      const addData = (dataArr: [number, number, string, boolean][], rowIndex: number, columnIndex: number, value: string): [number, number, string, boolean][] => {
        for (let i = 0; i < dataArr.length; i++) {
          const dataEntry = dataArr[i]
          if (dataEntry[0] === rowIndex && dataEntry[1] === columnIndex) {
            dataEntry[2] = value
            return dataArr
          }
        }

        dataArr.push([rowIndex, columnIndex, value, false])

        return dataArr
      } 
      for (let i = 0; i < 50; i++) {
        const rowIndex = randomIntRange(0, 99)
        const columnIndex = randomIntRange(0, 99)
        const value = randomStringGenerator(32)
        addData(data, rowIndex, columnIndex, value)
        mm.addColumnRow(rowIndex, columnIndex, value)
      }

      mm.forEachColumn((value, columnIndex, rowIndex) => {
        const dataEntryIndex = findInData(data, value)
        
        if (dataEntryIndex === -1) {
          console.error({value, columnIndex, rowIndex})
          return true
        }
        
        const dataEntry = data[dataEntryIndex]
        expect(rowIndex).toBe(dataEntry[0])
        expect(columnIndex).toBe(dataEntry[1])
        expect(value).toBe(dataEntry[2])

        dataEntry[3] = true
      })

      for (let i = 0; i < data.length; i++) expect(data[i][3]).toBe(true)
    })

    it("should be able to get max column length of column index", () => {
      const mm = new MatrixManager<string>()
      const checkColumnIndex = randomIntRange(0, 99)
      let maxLength = 0
      for (let i = 1; i < 50; i++) {
        const value = randomStringGenerator(randomIntRange(8, 64))
        mm.addColumnRow(i, checkColumnIndex, value)
        if (value.length > maxLength) maxLength = value.length
      }

      for (let i = 0; i < 50; i++) {
        const rowIndex = randomIntRange(0, 99)
        let columnIndex
        do {
          columnIndex = randomIntRange(0, 99)
        } while (columnIndex === checkColumnIndex)
        const value = randomStringGenerator(randomIntRange(8, 64))
        mm.addColumnRow(rowIndex, columnIndex, value)
      }
      
      expect(mm.getMaxColumnLength(checkColumnIndex)).toBe(maxLength)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The Matrix Manager Suite', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
