import { shouldRunSuite } from '../../SuiteRunChecker';
import { IStaticBuildProcess, EBuildProcessResultTypes } from '../../../src/Domain/Interfaces/BuildProcess';
import { randomIntRange, randomStringGenerator } from '@android4682/typescript-toolkit';
import { BuildStepManager } from '../../../src/Application/Classes/BuildStepManager';
import { generateStepResults, getDummyBuildSteps, DummyBuildStep } from "../../Helper/BuildStep";

if (shouldRunSuite('UNIT')) {
  describe('The BuildStepManager', () => {
    it('should be able to get all steps', () => {
      const manager = new BuildStepManager(getDummyBuildSteps())

      expect(manager.getAllSteps()).toStrictEqual(getDummyBuildSteps())
    })

    it('should be able to get a randomly choosen step', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      const stepIndex = randomIntRange(0, dummySteps.length-1)

      expect(manager.getStep(stepIndex)).toStrictEqual(dummySteps[stepIndex])
    })

    it('should be able to get a randomly choosen step by name', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      const step = dummySteps[randomIntRange(0, dummySteps.length-1)]

      expect(manager.getStepByName(step.name)).toStrictEqual(step)
    })

    it('should not be able to get a step that is out of bounds', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      expect(manager.getStep.bind(manager, dummySteps.length)).toThrow()
    })

    it('should not be able to get a step that isn\'t specified', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      expect(manager.getStepByName.bind(manager, randomStringGenerator(8, false))).toThrow()
    })

    it('should be able to be overwritten with any other array of IStaticBuildProcess', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)
      const overwrittenSteps: IStaticBuildProcess[] = [DummyBuildStep]

      manager.overrideAllSteps(overwrittenSteps)

      expect(manager.getAllSteps()).toStrictEqual(overwrittenSteps)
    })

    it('should not be able to get an invalid step result', () => {
      const dummyStepResults = generateStepResults()

      expect(BuildStepManager.getAnyValidStepResult.bind(BuildStepManager, dummyStepResults, ['UnknownName'], EBuildProcessResultTypes.VOID)).toThrow()
    })

    it('should not be able to get a fallback step result with invalid name and no fallback', () => {
      const dummyStepResults = generateStepResults()

      expect(BuildStepManager.getAnyValidStepResult.bind(BuildStepManager, dummyStepResults, ['InvalidName'], undefined)).toThrow()
    })

    it('should be able to get a fallback step result', () => {
      const dummyStepResults = generateStepResults(true)
      const fallbackDummyStepResult = dummyStepResults.get('DummyFallbackFileListName')

      expect(BuildStepManager.getAnyValidStepResult(dummyStepResults, ['DummyFileListName'], EBuildProcessResultTypes.FILE_LIST)).toBe(fallbackDummyStepResult)
    })

    it('should be able to get a valid step result', () => {
      const dummyStepResults = generateStepResults()
      const dummyStepResult = dummyStepResults.get('DummyFileListName')

      expect(BuildStepManager.getAnyValidStepResult(dummyStepResults, ['DummyFileListName'], EBuildProcessResultTypes.FILE_LIST)).toBe(dummyStepResult)
    })

    it('should be able to loop through step results', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      let loops = 0
      manager.forEach((buildProcess, type, stepNumber) => {
        loops++
      })

      expect(loops).toBe(dummySteps.length)
    })

    it('should be able to loop through step results as async', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      return new Promise<void>(async (resolve, reject) => {
        let loops = 0
        await manager.forEachAsync(async (buildProcess, type, stepNumber) => {
          loops++
        })
        
        expect(loops).toBe(dummySteps.length)
        resolve()
      })
    })

    it('should be able to break a loop through step results', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      let loops = 0
      manager.forEach((buildProcess, type, stepNumber) => {
        loops++
        return false
      })

      expect(loops).toBe(1)
    })

    it('should be able to break a loop through step results as async', () => {
      const dummySteps = getDummyBuildSteps()
      const manager = new BuildStepManager(dummySteps)

      return new Promise<void>(async (resolve, reject) => {
        let loops = 0
        await manager.forEachAsync(async (buildProcess, type, stepNumber) => {
          loops++
          return false
        })
        
        expect(loops).toBe(1)
        resolve()
      })
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('The BuildStepManager Suite', () => {
      it('should be skipped because their are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
