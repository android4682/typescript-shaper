import type { Config } from "@jest/types"

const config: Config.InitialOptions = {
  preset: "ts-jest",
  testEnvironment: "node",
  automock: false,
  coverageReporters: [
    'html',
    'text',
    'text-summary',
    'cobertura'
  ],
  globals: {
    'ts-jest': {
      tsconfig: './specs/tsconfig.json'
    }
  },
  "testRegex": ".\/specs\/.*\.(?:spec|test)\.(?:t|j)s$",
  "coverageThreshold": {
    "global": {
      "branches": 0,
      "functions": 0,
      "lines": 0,
      "statements": 0
    }
  },
  "collectCoverageFrom": [
    "./src/**/*.(t|j)s",
    "!./src/Infrastructure/index.ts",
    "!./src/build.ts",
    "!./src/Domain/**"
  ],
  moduleDirectories: [
    'node_modules',
    'src'
  ],
  "moduleFileExtensions": [
    "ts",
    "js",
    "json"
  ],
  "transform": {
    "^.+\\.(t|j)s$": "ts-jest"
  },
  "coverageDirectory": "./coverage/jest",
  coverageProvider: "babel"
}
export default config
