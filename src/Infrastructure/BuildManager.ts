import { BetterConsole, getMessageAndStack } from "@android4682/better-console";
import { SpecialMap } from "@android4682/typescript-toolkit";
import { BaseHelpBuildArgumentModule } from "../Application/Base/BaseHelpModule";
import { BaseOptionDebugModule } from "../Application/Base/BaseOptionDebugModule";
import { BuilderArgument } from "../Application/Arguments/Global/BuilderArgument";
import { HelpArgument } from "../Application/Arguments/Global/HelpArgument";
import { ShowOptionsArgument } from "../Application/Arguments/Global/ShowOptionsArgument";
import { VersionArgument } from "../Application/Arguments/Global/VersionArgument";
import { HelpModuleManager } from "../Application/Classes/HelpModuleManager";
import { OptionDebugModuleManager } from "../Application/Classes/OptionDebugModuleManager";
import { IStaticBuilder, TBuildArguments } from "../Domain/Interfaces/Builder";
import { IBuilderArgument } from "../Domain/Interfaces/BuilderArgument";
import { SingleFileBuilder } from "./Builders/SingleFileBuilder";
import { ProgramInfoHelper } from "../Application/Classes/ProgramInfoHelper";
import { ValidBuildersArgument } from "../Application/Arguments/Global/ValidBuildersArgument";
import { FivemClientBuilder } from "./Builders/FivemClientBuilder";
import { FivemPrecompiledAutowireBuilder } from "./Builders/FivemPrecompiledAutowireBuilder";

export class BuildManager {
  protected static builders: SpecialMap<string, IStaticBuilder> = new SpecialMap<string, IStaticBuilder>()
  protected static globalArguments: TBuildArguments = new SpecialMap<string, IBuilderArgument>()
  private static initiated: boolean = false
  public static exitOnDone: boolean = true
  private static betterConsole: BetterConsole = BetterConsole.Create()

  public static initiate(): boolean
  {
    if (this.initiated) return this.initiated
    
    this.registerGlobalArgument(new BuilderArgument())
    this.registerGlobalArgument(new HelpArgument())
    this.registerGlobalArgument(new ShowOptionsArgument())
    this.registerGlobalArgument(new VersionArgument())
    this.registerGlobalArgument(new ValidBuildersArgument())

    this.registerBuilder(SingleFileBuilder)
    this.registerBuilder(FivemClientBuilder)
    this.registerBuilder(FivemPrecompiledAutowireBuilder)

    return this.initiated = true
  }

  public static getAllBuilders(): SpecialMap<string, IStaticBuilder>
  {
    return this.builders
  }

  public static getBuilder(name: string): IStaticBuilder | undefined
  {
    return this.builders.get(name)
  }

  public static registerBuilder(builder: IStaticBuilder): void
  {
    this.builders.set(builder.builder_name, builder)
  }

  public static getGlobalArgument(name: string): IBuilderArgument | undefined
  {
    return this.globalArguments.get(name)
  }

  public static getAllGlobalArgument(): TBuildArguments
  {
    return this.globalArguments
  }

  public static registerGlobalArgument(argument: IBuilderArgument): void
  {
    this.globalArguments.set(argument.constructor.name, argument)
  }

  protected static printFooter(): void
  {
    this.betterConsole.info(`\n${ProgramInfoHelper.getFooter()}`)
  }

  public static getApplicationHelpText(target: 'GLOBAL' | 'BUILDER' = 'BUILDER'): string
  {
    return HelpModuleManager.formatModules(BaseHelpBuildArgumentModule.fromBatchBuildArgument(this.globalArguments.valuesAsArray(), true), target)
  }

  protected static printApplicationHelpText(text: string = this.getApplicationHelpText('GLOBAL')): void
  {
    this.betterConsole.log(text)
    this.printFooter()
  }

  public static getApplicationOptionsText(): string
  {
    return OptionDebugModuleManager.formatModules(BaseOptionDebugModule.fromBatchBuildArgument(this.globalArguments.valuesAsArray()))
  }

  protected static printApplicationOptionsText(text: string = this.getApplicationOptionsText()): void
  {
    this.betterConsole.log(text)
    this.printFooter()
  }

  protected static printApplicationVersion(text: string = ProgramInfoHelper.getVersion()): void
  {
    this.betterConsole.log(text)
  }

  protected static printValidBuilders(): void
  {
    let text = `Usage: ${ProgramInfoHelper.getShortRelativeBinRunner()} -b <builder_name>\n\nValid builders:`
    this.builders.forEach((builder, name) => {
      text += `\n- ${name}`
    })

    this.betterConsole.info(text)
    this.printFooter()
  }

  public static async main(): Promise<void>
  {
    this.initiate()
    
    const builderArgument = new BuilderArgument()
    builderArgument.process()
    
    let staticBuilder = this.getBuilder(builderArgument.value)
    
    const globalHelperArgument = this.getGlobalArgument(HelpArgument.name)
    const globalShowOptionsArgument = this.getGlobalArgument(ShowOptionsArgument.name)
    const globalVersionArgument = this.getGlobalArgument(VersionArgument.name)
    const globalValidBuildersArgument = this.getGlobalArgument(ValidBuildersArgument.name)
    if (
      ! globalHelperArgument
      || ! globalShowOptionsArgument
      || ! globalVersionArgument
      || ! globalValidBuildersArgument
    ) {
      throw new Error('NOBODY EXPECTS THE SPANISH INQUISITION!')
    }
    

    if (! globalVersionArgument.process()) this.betterConsole.info(ProgramInfoHelper.getHeader() + '\n')
    
    if (staticBuilder) {
      this.betterConsole.info('Picked builder: ' + staticBuilder.builder_name)
      if (globalHelperArgument.process()) {
        this.printApplicationHelpText(staticBuilder.getHelpText())
      } else if (globalShowOptionsArgument.process()) {
        this.printApplicationOptionsText(staticBuilder.getOptionsText())
      } else {
        try {
          await new staticBuilder().runBuild()
          this.printFooter()
        } catch (e) {
          let [message, stack] = getMessageAndStack(e)
          this.betterConsole.error(message)
          this.betterConsole.error(stack)
          this.betterConsole.error(`\nIf the above error didn't help, try adding '-h' to the command you're trying to run.`)
          if (this.exitOnDone) process.exit(1)
        }
  	  }
    } else {
      if (globalShowOptionsArgument.process()) this.printApplicationOptionsText()
      else if (globalVersionArgument.value) this.printApplicationVersion()
      else if (globalHelperArgument.process()) this.printApplicationHelpText()
      else if (globalValidBuildersArgument.process()) this.printValidBuilders()
      else {
        this.betterConsole.error(`No builder has been set/found and no global arguments have been passed. Try '${globalHelperArgument.Static.argumentSelector.join("' or '")}' to see the help text.`)
        if (this.exitOnDone) process.exit(1)
      }
    }

    if (this.exitOnDone) process.exit(0)
  }
}
