import { StaticImplements } from "@android4682/typescript-toolkit";
import { AbstractBuilder } from "../../Application/Base/Abstract/AbstractBuilder";
import { BuildProcessedFileContentProcess } from "../../Application/BuildProcesses/BuildProcessedFileContentProcess";
import { GetAllFilesProcess } from "../../Application/BuildProcesses/GetAllFilesProcess";
import { GetAllImportsProcess } from "../../Application/BuildProcesses/GetAllImportsProcess";
import { JoinFilesProcess } from "../../Application/BuildProcesses/JoinFilesProcess";
import { MoveCompiledFilesToDistProcess } from "../../Application/BuildProcesses/MoveCompiledFilesToDistProcess";
import { PostCleanupProcess } from "../../Application/BuildProcesses/PostCleanupProcess";
import { PreCleanupProcess } from "../../Application/BuildProcesses/PreCleanupProcess";
import { PrepandImportsProcess } from "../../Application/BuildProcesses/PrepandImportsProcess";
import { PreparePathsProcess } from "../../Application/BuildProcesses/PreparePathsProcess";
import { RemoveAllImportsProcess } from "../../Application/BuildProcesses/RemoveAllImportsProcess";
import { SortBuildFilesProcess } from "../../Application/BuildProcesses/SortBuildFilesProcess";
import { BuildStepManager } from "../../Application/Classes/BuildStepManager";
import { NodeTypeScriptCompiler } from "../../Application/Compilers/NodeTypeScriptCompiler";
import { IStaticBuilder } from "../../Domain/Interfaces/Builder";
import { LoadAutowiringClassesProcess } from "../../Application/BuildProcesses/LoadAutowiringClassesProcess";
import { CreatePreCompiledAutoWireMappingProcess } from "../../Application/BuildProcesses/CreatePreCompiledAutoWireMappingProcess";

export type TStaticFivemPrecompiledAutowireBuilder = StaticImplements<IStaticBuilder, typeof FivemPrecompiledAutowireBuilder>

export class FivemPrecompiledAutowireBuilder extends AbstractBuilder implements TStaticFivemPrecompiledAutowireBuilder
{
  public static readonly builder_name: string = 'FivemPrecompiledAutowireBuilder'

  public static readonly pre_build_steps: BuildStepManager = new BuildStepManager([
    PreCleanupProcess,
    PreparePathsProcess
  ])

  public static readonly build_steps: BuildStepManager = new BuildStepManager([
    GetAllFilesProcess,
    SortBuildFilesProcess,
    JoinFilesProcess,
    GetAllImportsProcess,
    RemoveAllImportsProcess,
    PrepandImportsProcess,
    BuildProcessedFileContentProcess,
    LoadAutowiringClassesProcess
  ])
  
  public static readonly compiler_steps: BuildStepManager = new BuildStepManager([
    NodeTypeScriptCompiler,
    MoveCompiledFilesToDistProcess,
    CreatePreCompiledAutoWireMappingProcess
  ]);
  
  public static readonly post_build_steps: BuildStepManager = new BuildStepManager([
    PostCleanupProcess
  ])
}
