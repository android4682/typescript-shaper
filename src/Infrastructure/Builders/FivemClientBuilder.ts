import { StaticImplements } from "@android4682/typescript-toolkit";
import { AbstractBuilder } from "../../Application/Base/Abstract/AbstractBuilder";
import { BuildProcessedFileContentProcess } from "../../Application/BuildProcesses/BuildProcessedFileContentProcess";
import { GetAllFilesProcess } from "../../Application/BuildProcesses/GetAllFilesProcess";
import { GetAllImportsProcess } from "../../Application/BuildProcesses/GetAllImportsProcess";
import { JoinFilesProcess } from "../../Application/BuildProcesses/JoinFilesProcess";
import { MoveCompiledFilesToDistProcess } from "../../Application/BuildProcesses/MoveCompiledFilesToDistProcess";
import { PostCleanupProcess } from "../../Application/BuildProcesses/PostCleanupProcess";
import { PreCleanupProcess } from "../../Application/BuildProcesses/PreCleanupProcess";
import { PrepandImportsProcess } from "../../Application/BuildProcesses/PrepandImportsProcess";
import { PreparePathsProcess } from "../../Application/BuildProcesses/PreparePathsProcess";
import { RemoveAllImportsProcess } from "../../Application/BuildProcesses/RemoveAllImportsProcess";
import { SortBuildFilesProcess } from "../../Application/BuildProcesses/SortBuildFilesProcess";
import { BuildStepManager } from "../../Application/Classes/BuildStepManager";
import { NodeTypeScriptCompiler } from "../../Application/Compilers/NodeTypeScriptCompiler";
import { IStaticBuilder } from "../../Domain/Interfaces/Builder";

export type TStaticFivemClientBuilder = StaticImplements<IStaticBuilder, typeof FivemClientBuilder>

export class FivemClientBuilder extends AbstractBuilder implements TStaticFivemClientBuilder
{
  public static readonly builder_name: string = 'FivemClientBuilder'

  public static readonly pre_build_steps: BuildStepManager = new BuildStepManager([
    PreCleanupProcess,
    PreparePathsProcess
  ])

  public static readonly build_steps: BuildStepManager = new BuildStepManager([
    GetAllFilesProcess,
    SortBuildFilesProcess,
    JoinFilesProcess,
    GetAllImportsProcess,
    RemoveAllImportsProcess,
    BuildProcessedFileContentProcess
  ])
  
  public static readonly compiler_steps: BuildStepManager = new BuildStepManager([
    NodeTypeScriptCompiler,
    MoveCompiledFilesToDistProcess
  ]);
  
  public static readonly post_build_steps: BuildStepManager = new BuildStepManager([
    PostCleanupProcess
  ])
}
