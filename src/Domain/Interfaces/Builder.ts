import { SpecialMap } from "@android4682/typescript-toolkit";
import { IBuilderArgument, IStaticBuilderArgument } from "./BuilderArgument";
import { IBuildStepManager } from "./BuildStepManager";

export interface IBuilder
{
  runBuild(): Promise<void>
}

export type TBuildArguments = SpecialMap<string, IBuilderArgument>

export interface IStaticBuilder
{
  new(): IBuilder
  readonly builder_name: string
  readonly pre_build_steps: IBuildStepManager
  readonly build_steps: IBuildStepManager
  readonly compiler_steps: IBuildStepManager
  readonly post_build_steps: IBuildStepManager
  getHelpText(): string
  getOptionsText(): string
}
