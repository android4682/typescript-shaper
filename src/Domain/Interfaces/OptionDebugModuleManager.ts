import { IOptionDebugModule } from "./OptionDebugModule";

export interface IStaticOptionDebugModuleManager
{
  new (): any
  formatModules(modules: IOptionDebugModule[]): string
}