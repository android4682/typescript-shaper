export interface AutowireCategories
{
  [category: string]: string[]
}