import { IStaticBuildProcess } from "./BuildProcess";

export type TBuildStep = IStaticBuildProcess

export interface IBuildStepManager
{
  getAllSteps(): TBuildStep[]
  overrideAllSteps(steps: TBuildStep[]): this
  getStep(step: number): TBuildStep
}
