export interface AutowireConfig
{
  [category: string]: string | Array<string>
}