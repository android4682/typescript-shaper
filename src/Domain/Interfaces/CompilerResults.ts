export interface ICompilerResults
{
  jsFiles: string[]
  sourceMapFiles: string[]
  declarationFiles: string[]
}
