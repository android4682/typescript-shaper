import { SortableBuildFile } from "../../Application/Classes/BuildFileSorter";

export type CustomSorterFunc<C extends SortableBuildFile = SortableBuildFile> = (sortedItems: C[], alreadySorted: C[]) => number | null

export type BuildOrder = {
  [file: string]: CustomSorterFunc
}
