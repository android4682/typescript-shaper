import { IHelpBuildArgumentModule } from "./HelpModules";

export interface IStaticHelpModuleManager
{
  new (): any
  formatModules(modules: IHelpBuildArgumentModule[]): string
}
