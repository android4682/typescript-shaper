import { IBuilderArgument } from "./BuilderArgument";

export interface IHelpBuildArgumentModule
{
  buildArgument: IBuilderArgument
  requiredBy: string[]
  isRequired: boolean
  addRequiredBy(processName: string): this
  usedBy: string[]
  isUsedBy(processName: string): boolean
  addUsedBy(processName: string): this
  argumentSelector: string[]
  passingArguments: string[]
  description: string[]
  defaultValue: string
}
