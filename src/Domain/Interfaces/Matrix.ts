export type TMatrix<T> = TMatrixRow<T>[]
export type TMatrixRow<T> = TMatrixColumn<T>[]
export type TMatrixColumn<T> = T