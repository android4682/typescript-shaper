export interface IBuilderArgument
{
  value: string | boolean | string[] | undefined
  Static: IStaticBuilderArgument
  process(): string | boolean | string[]
}

export interface IStaticBuilderArgument
{
  new(value?: any): IBuilderArgument
  findArgumentSelectorIndex(): number
  readonly defaultValue: string | string[] | boolean
  readonly argumentSelector: string[]
  readonly passingArguments: string[]
  helpDescription: string[]
}
