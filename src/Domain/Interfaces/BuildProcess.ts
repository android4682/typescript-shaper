import { SpecialMap } from "@android4682/typescript-toolkit";
import { IStaticBuilderArgument } from "./BuilderArgument";

export interface ImportData {
  static: {
    [plugin: string]: string | undefined
  }
  dynamic: {
    [plugin: string]: string[] 
  }
}

export interface IBuildProcess
{
  process(stepResults: TBuildProcessResults): any
}

export type TBuildProcessRequiredResult = [string[], EBuildProcessResultTypes]
export interface IStaticBuildProcess
{
  new(...args: any[]): IBuildProcess
  readonly returnType: EBuildProcessResultTypes
  readonly requiredArguments: IStaticBuilderArgument[]
  readonly optionalArguments: IStaticBuilderArgument[]
  readonly requiredResults: TBuildProcessRequiredResult[]
}

export type TBuildProcessResults = SpecialMap<string, IBuildProcessResult<any>>

export enum EBuildProcessResultTypes 
{
  'VOID',
  'FILE_LIST',
  'FILE_CONTENT',
  'IMPORTS',
  'BUILD_FILE_PATH',
  'COMPILER_RESULTS',
  'BUILD_ORDER_FILE',
  'AUTOWIRE_CLASS_NAMES'
}

export type TBuildProcessResultTypes = EBuildProcessResultTypes | string & {}

export interface IBuildProcessResult<RESULT_TYPE>
{
  readonly type: string | number
  readonly results: RESULT_TYPE
}