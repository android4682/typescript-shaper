import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class AddCwdToDistPathArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--dist-add-cwd'];
  public static readonly helpDescription: string[] = ['Prepends the current CWD to the overwriten dist folder argument.'];
}