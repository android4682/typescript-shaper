import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class VerboseArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['-v', '--verbose'];
  public static readonly helpDescription: string[] = ['Enables verbose.'];
}