import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class SkipPrebuildCleanupArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--skip-prebuild-cleanup'];
  public static readonly helpDescription: string[] = ['Don\'t clean up before building.'];
}