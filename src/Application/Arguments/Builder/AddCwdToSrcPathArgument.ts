import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class AddCwdToSrcPathArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--src-add-cwd'];
  public static readonly helpDescription: string[] = ['Prepends the current CWD to the overwriten src folder argument.'];
}