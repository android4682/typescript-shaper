import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";

export class SourceFolderArgument extends BasePathValueBuildArgument
{
  public static readonly defaultValue: string = './src'
  public static readonly argumentSelector: string[] = ['--src-folder'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = ['Overrides the default place to use as source folder.'];
}
