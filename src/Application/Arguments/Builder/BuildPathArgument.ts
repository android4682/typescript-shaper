import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";

export class BuildPathArgument extends BasePathValueBuildArgument
{
  public static readonly defaultValue: string = './build'
  public static readonly argumentSelector: string[] = ['--build-path'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = ['Overrides build path.'];
}
