import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class IgnoreDeclarationFilesInSourceFolderArgument extends BaseBooleanBuildArgument
{
  public static readonly defaultValue: boolean = false;
  public static readonly argumentSelector: string[] = ['--ignore-src-declaration-files'];
  public static readonly helpDescription: string[] = ['Copies *.d.ts files found in source folder to dist folder.'];
}