import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class EnableTypeBuildingArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--enable-type-building'];
  public static readonly helpDescription: string[] = ['Enables declaration building, overriding tsconfig\'s declaration setting.'];
}