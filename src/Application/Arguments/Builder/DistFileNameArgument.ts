import { BaseSingleStringValueBuildArgument } from "../../Base/BuildArguments/BaseSingleStringValueBuildArgument";

export class DistFileNameArgument extends BaseSingleStringValueBuildArgument
{
  public static readonly defaultValue: string = 'index.js'
  public static readonly argumentSelector: string[] = ['--dist-filename'];
  public static readonly passingArguments: string[] = ['<filename>']
  public static readonly helpDescription: string[] = ['Sets a custom dist filename.'];
}
