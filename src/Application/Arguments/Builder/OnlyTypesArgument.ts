import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class OnlyTypesArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--only-types'];
  public static readonly helpDescription: string[] = ['Export only declaration files.'];
}