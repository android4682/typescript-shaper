import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class EnableSourceMapBuildingArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--enable-sourcemap-building'];
  public static readonly helpDescription: string[] = ['Enables sourcemap building, overriding tsconfig\'s sourcemap setting.'];
}