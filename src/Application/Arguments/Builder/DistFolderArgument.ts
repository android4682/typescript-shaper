import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";

export class DistFolderArgument extends BasePathValueBuildArgument
{
  public static readonly defaultValue: string = './dist'
  public static readonly argumentSelector: string[] = ['--dist-folder'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = ['Overrides the default place to use as dist folder'];
}
