import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class OnlyJoinArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--only-join'];
  public static readonly helpDescription: string[] = ['Only joins files, exits after.'];
}