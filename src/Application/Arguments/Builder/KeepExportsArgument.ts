import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class KeepExportsArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--keep-exports'];
  public static readonly helpDescription: string[] = ['Keeps export statements.'];
}