import { BaseSingleStringValueBuildArgument } from "../../Base/BuildArguments/BaseSingleStringValueBuildArgument";
import { BaseStringValueBuildArgument } from "../../Base/BuildArguments/BaseStringValueBuildArgument";

export class SubFolderArgument extends BaseSingleStringValueBuildArgument
{
  public static readonly defaultValue: string = 'shared'
  public static readonly argumentSelector: string[] = ['--subfolder'];
  public static readonly passingArguments: string[] = ['<folder_name>']
  public static readonly helpDescription: string[] = ['Search in/adds a subfolder with the given name'];
}
