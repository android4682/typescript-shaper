import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";
import { DistFolderArgument } from "./DistFolderArgument";

export class PreCompiledAutoWireBuildPathArgument extends BasePathValueBuildArgument
{
  public static readonly defaultValue: string = '{{DIST_FOLDER}}/autowire_classes.js'
  public static readonly argumentSelector: string[] = ['--dist-compiled-autowire'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = [
    'The path to the autowire config file.',
    'A json file with a name (string) path (string value) object.',
    'You can use {{DIST_FOLDER}} to define your dist folder given by ' + DistFolderArgument.argumentSelector[0]
  ];

  public get value(): string
  {
    const arg = new DistFolderArgument()
    
    return super.value.replace('{{DIST_FOLDER}}', arg.process())
  }
}
