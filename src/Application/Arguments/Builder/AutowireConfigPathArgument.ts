import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";
import { SourceFolderArgument } from "./SourceFolderArgument";

export class AutowireConfigPathArgument extends BasePathValueBuildArgument
{
  public static readonly defaultValue: string = '{{CWD}}/autowire_config.json'
  public static readonly argumentSelector: string[] = ['--autowire-config'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = [
    'The path to the autowire config file.',
    'A json file with a name (string) path (string value) object.',
    'You can use {{SRC}} to define your source folder given by ' + SourceFolderArgument.argumentSelector[0]
  ];
}
