import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class SkipCleanupArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--skip-cleanup'];
  public static readonly helpDescription: string[] = ['Don\'t clean up after building.'];
}