import path from "node:path";
import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";
import { DistFolderArgument } from "./DistFolderArgument"

export class TypesPathArgument extends BasePathValueBuildArgument
{
  public readonly _defaultValueOverride: string
  public static readonly defaultValue: string = DistFolderArgument.defaultValue
  public static readonly argumentSelector: string[] = ['--types-path'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = ['Path to place .d.ts files.'];

  public constructor(value?: string)
  {
    super()

    this._defaultValueOverride = path.join(new DistFolderArgument().process())
  }

  public get value(): string
  {
    return this._value || this._defaultValueOverride
  }
}
