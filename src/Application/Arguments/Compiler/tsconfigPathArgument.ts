import path from "path";
import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";
import { SourceFolderArgument } from "../Builder/SourceFolderArgument";

export class TsconfigPathArgument extends BasePathValueBuildArgument
{
  private _defaultValueOverride: string
  public static readonly defaultValue: string = './src/tsconfig.json'
  public static readonly argumentSelector: string[] = ['--tsconfig-path'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = ['Path to tsconfig.json file.'];

  public constructor(value?: string)
  {
    super()

    this._defaultValueOverride = path.join(new SourceFolderArgument().process(), 'tsconfig.json')
  }

  public get value(): string
  {
    return this._value || this._defaultValueOverride
  }
}
