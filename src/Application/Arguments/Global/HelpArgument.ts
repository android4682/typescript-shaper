import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class HelpArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['-h', '--help'];
  public static readonly helpDescription: string[] = ['Shows help information.'];
}