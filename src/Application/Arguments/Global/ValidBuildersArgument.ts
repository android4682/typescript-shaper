import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class ValidBuildersArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['-l', '--show-builders'];
  public static readonly helpDescription: string[] = ['Prints a list of valid builders.'];
}