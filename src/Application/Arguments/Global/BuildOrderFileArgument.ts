import path from "path";
import { BasePathValueBuildArgument } from "../../Base/BuildArguments/BasePathValueBuildArgument";
import { SourceFolderArgument } from "../Builder/SourceFolderArgument";

export class BuildOrderFileArgument extends BasePathValueBuildArgument
{
  private readonly _defaultValueOverride: string
  public static readonly defaultValue: string = './src/build_order.js'
  public static readonly argumentSelector: string[] = ['--build-order-path'];
  public static readonly passingArguments: string[] = ['<path>']
  public static readonly helpDescription: string[] = ['Path to place .d.ts files.'];

  public constructor(value?: string)
  {
    super()

    this._defaultValueOverride = path.join(new SourceFolderArgument().process(), 'build_order.js')
  }

  public get value(): string
  {
    return this._value || this._defaultValueOverride
  }
}
