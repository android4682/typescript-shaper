import { BaseSingleStringValueBuildArgument } from "../../Base/BuildArguments/BaseSingleStringValueBuildArgument";

export class BuilderArgument extends BaseSingleStringValueBuildArgument
{
  public static readonly defaultValue: string = ''
  public static readonly argumentSelector: string[] = ['-b', '--builder'];
  public static readonly passingArguments: string[] = ['<builder_name>']
  public static readonly helpDescription: string[] = ['Sets the builder to use. This is required!'];
}
