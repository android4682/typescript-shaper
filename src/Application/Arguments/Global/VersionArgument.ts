import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class VersionArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--version'];
  public static readonly helpDescription: string[] = ['Prints only the version of this application.'];
}