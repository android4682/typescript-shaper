import { BaseBooleanBuildArgument } from "../../Base/BuildArguments/BaseBooleanBuildArgument";

export class ShowOptionsArgument extends BaseBooleanBuildArgument
{
  public static readonly argumentSelector: string[] = ['--show-options'];
  public static readonly helpDescription: string[] = ['Displays all the currently available arguments values and what they are currently set to.', 'This is mainly used for debugging building options.'];
}