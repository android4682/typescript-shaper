export class ChildrenMismatchException extends Error {
  public name: string = this.constructor.name;

  public static default(inPlace: string, element: string = 'length'): ChildrenMismatchException
  {
    return new this(`Children '${element}' mismatch in '${inPlace}'.`)
  }
}