export class MissingArgumentsException extends Error {
  public name: string = this.constructor.name;

  public static createDefault(argument?: string): MissingArgumentsException
  {
    return new this(`Missing ${(argument) ? `${argument} ` : ''}argument.`)
  }

  public static fromBuildProcess(processName: string, argumentName: string): MissingArgumentsException
  {
    return new this(`Missing builder argument '${argumentName}' for '${processName}'.`)
  }
}