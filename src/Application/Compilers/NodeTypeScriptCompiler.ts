import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractCompiler } from "../Base/Abstract/AbstractCompiler";
import tsc from "@android4682/node-typescript-compiler-with-declaration";
import fs from "node:fs";
import { BuildPathArgument } from "../Arguments/Builder/BuildPathArgument";
import { TsConfig } from "../../Domain/Libraries/tsconfig";
import { TsconfigPathArgument } from "../Arguments/Compiler/tsconfigPathArgument";
import { EnableTypeBuildingArgument } from "../Arguments/Builder/EnableTypeBuildingArgument";
import path from "node:path";
import { VerboseArgument } from "../Arguments/Builder/VerboseArgument";
import { ICompilerResults } from "../../Domain/Interfaces/CompilerResults";
import { BaseCompilerResult } from "../Base/BaseCompilerResults";
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { BuildProcessedFileContentProcess } from "../BuildProcesses/BuildProcessedFileContentProcess";
import { BetterConsole } from "@android4682/better-console";
import { EnableSourceMapBuildingArgument } from "../Arguments/Builder/EnableSourceMapBuildingArgument";

export class NodeTypeScriptCompiler extends AbstractCompiler
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    BuildPathArgument,
    TsconfigPathArgument,
  ];
  
  public static readonly optionalArguments: IStaticBuilderArgument[] = [
    EnableTypeBuildingArgument,
    EnableSourceMapBuildingArgument
  ]

  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[BuildProcessedFileContentProcess.name], EBuildProcessResultTypes.BUILD_FILE_PATH]
  ];
  
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.COMPILER_RESULTS

  public async process(stepResults: TBuildProcessResults): Promise<ICompilerResults>
  {
    super.process(stepResults)
    const buildPathArgument = <BuildPathArgument> this.getArgument(BuildPathArgument)
    const tsconfigPathArgument = <TsconfigPathArgument> this.getArgument(TsconfigPathArgument)
    const enableTypeBuildingArgument = <EnableTypeBuildingArgument> this.getArgument(EnableTypeBuildingArgument)
    const enableSourceMapBuildingArgument = <EnableSourceMapBuildingArgument> this.getArgument(EnableSourceMapBuildingArgument)

    const buildFilePath = <IBuildProcessResult<string>> this.getResult(stepResults, EBuildProcessResultTypes.BUILD_FILE_PATH)
    
    BetterConsole.Create().info("Compiling typescript...")

    let tsconfig: TsConfig;
    try {
      tsconfig = JSON.parse(fs.readFileSync(tsconfigPathArgument.value).toString())
  
      delete tsconfig.compilerOptions.outDir
      delete tsconfig.compilerOptions.outFile
      if (enableTypeBuildingArgument.value) tsconfig.compilerOptions.declaration = true
      if (enableSourceMapBuildingArgument.value) tsconfig.compilerOptions.sourceMap = true

      await tsc.compile(tsconfig.compilerOptions, [buildFilePath.results], {verbose: new VerboseArgument().process()})

      let basePathName = path.join(buildPathArgument.value, path.basename(buildFilePath.results, '.ts'))
      let compilerResults = new BaseCompilerResult([basePathName + '.js'])

      if (tsconfig.compilerOptions.sourceMap) {
        compilerResults.sourceMapFiles.push(basePathName + '.js.map')
      }

      if (tsconfig.compilerOptions.declaration) {
        compilerResults.declarationFiles.push(basePathName + '.d.ts')
      }

      return compilerResults
    } catch (e) {
      let err: Error = <Error> e
      if (err.message.includes('Unexpected token')) throw new Error('tsconfig has invalid json or couldn\'t be found at ' + tsconfigPathArgument.value)
      throw err
    }
  }
}