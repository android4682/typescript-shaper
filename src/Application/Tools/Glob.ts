import { glob } from "glob";
import path from "node:path"

export function globPromise(pattern: string): Promise<string[]>
{
  return new Promise<string[]>((resolve, reject) => {
    glob(pattern, (err, matches) => {
      if (err) reject(err)
      else resolve(matches)
    })
  })
}

export function fixGlob(globPatterns: string[], sourceFolder: string): string[]
{
  const result: string[] = []
  for (let i = 0; i < globPatterns.length; i++) {
    const globPattern = globPatterns[i].split(path.win32.sep).join(path.posix.sep)
    result.push(path.posix.join(sourceFolder, globPattern))
  }

  return result
}
