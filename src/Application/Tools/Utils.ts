import fs from "node:fs"

function filterByRegexp(content: string, re: RegExp): string | null
{
  const classNameMatches = re.exec(content)

  if (classNameMatches === null) return null
  
  return classNameMatches[1]
}

function getFilteredResultsFromFile(filePath: string, re: RegExp): string[]
{
  const results: string[] = []
  const content = fs.readFileSync(filePath).toString()
  let match;
  while ((match = re.exec(content)) !== null) {
    if (match[1] === null) continue

    results.push(match[1])
  }

  return results
}

export function getTypesNames(filePath: string): string[]
{
  return getFilteredResultsFromFile(filePath, new RegExp(/^(?:export\s)?type (.*?)(?:\<.*?)?\s?=/, 'gm'))
}

export function getInterfaceNames(filePath: string): string[]
{
  return getFilteredResultsFromFile(filePath, new RegExp(/^(?:export\s)?(?:interface|enum) (.*?)(?:\n|\r\n|\s|<)/, 'gm'))
}

export function getClassNames(filePath: string): string[]
{
  return getFilteredResultsFromFile(filePath, new RegExp(/^(?:export\s)?(?:abstract )?class (.*?)(?:\n|\r\n|\s|<)/, 'gm'))
}

export function getImportsFromNonModules(filePath: string): string[]
{
  const searchResults = getFilteredResultsFromFile(filePath, new RegExp(/^import\s?{\s?(.*?)\s?}\s?from\s?(?:"|')\./, 'gm'))

  const results: string[] = []
  for (let i = 0; i < searchResults.length; i++) {
    const result = searchResults[i];
    const elements = result.split(',')
    for (let j = 0; j < elements.length; j++) {
      const element = elements[j];
      results.push(element.trim())
    }
  }

  return results
}

export function getClassOrInterfaceDependencies(filePath: string): string[]
{
  return getFilteredResultsFromFile(filePath, new RegExp(/(?:^(?:export\s)?class (?:.*?)\s?(?:extends|implements)\s?(.*?)(?:\n|\r\n|\s|<)|:\s?)/, 'gm'))
}

export function getLooseFunctionNames(filePath: string): string[]
{
  return getFilteredResultsFromFile(filePath, new RegExp(/^(?:export\s)?function (.*?)(?:<|\s?\()/, 'gm'))
}

export function getExportedConsts(filePath: string): string[]
{
  return getFilteredResultsFromFile(filePath, new RegExp(/^export const (.*?)(?:\:.*?)?(?:\s?=)/, 'gm'))
}

export function deepEqual<T>(arr1: T[], arr2: T[]): boolean
{
  if (arr1.length !== arr2.length) {
    return false;
  }

  for (let i = 0; i < arr1.length; i++) {
    const element1 = arr1[i]
    const element2 = arr2[i]

    if (typeof element1 === 'object' && element1 !== null && element1 !== undefined && typeof element2 === 'object' && element2 !== null && element2 !== undefined) {
      if (! Array.isArray(element1) && ! Array.isArray(element2) && ! deepEqualObject(element1, element2)) return false
      else if (Array.isArray(element1) && Array.isArray(element2) && ! deepEqual(element1, element2)) return false
    } else if (element1 !== element2) return false;
  }

  return true;
}

export function deepEqualObject<T extends object>(obj1: T, obj2: T): boolean
{
  if (Object.keys(obj1).length !== Object.keys(obj2).length) return false

  for (const key in obj1) {
    if (Object.prototype.hasOwnProperty.call(obj1, key)) {
      if (! Object.prototype.hasOwnProperty.call(obj2, key)) return false

      const element1 = obj1[key]
      const element2 = obj2[key]

      if (typeof element1 === 'object' && element1 !== null && element1 !== undefined && typeof element2 === 'object' && element2 !== null && element2 !== undefined) {
        if (! Array.isArray(element1) && ! Array.isArray(element2) && ! deepEqualObject(element1, element2)) return false
        else if (Array.isArray(element1) && Array.isArray(element2) && ! deepEqual(element1, element2)) return false
      } else if (element1 !== element2) return false
    }
  }

  return true
}
