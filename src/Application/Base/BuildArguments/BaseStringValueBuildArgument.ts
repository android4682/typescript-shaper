import { AbstractBuildArgument } from "../Abstract/AbstractBuildArgument";

export abstract class BaseStringValueBuildArgument extends AbstractBuildArgument
{
  protected _value: string | string[] | undefined = undefined;
  public static readonly defaultValue: string = ''
  
  public constructor(value?: string | string[] | undefined)
  {
    super(value)
  }

  public get value(): string | string[]
  {
    return <string | string[]> super.value
  }

  public set value(value: string | string[])
  {
    this._value = value
  }

  public process(): string | string[]
  {
    return <string | string[]> super.process()
  }
}