import path from 'node:path'
import { BaseSingleStringValueBuildArgument } from './BaseSingleStringValueBuildArgument';

export abstract class BasePathValueBuildArgument extends BaseSingleStringValueBuildArgument
{
  public process(): string
  {
    super.process()
    if (!! this.value) this._value = path.join(this.value)

    return this.value
  }
}