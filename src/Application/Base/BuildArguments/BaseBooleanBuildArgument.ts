import { AbstractBuildArgument } from "../Abstract/AbstractBuildArgument";

export class BaseBooleanBuildArgument extends AbstractBuildArgument
{
  protected _value: boolean | undefined = undefined;
  public static readonly defaultValue: boolean = false;
  
  public constructor(value?: boolean)
  {
    super(value)
  }

  public get value(): boolean
  {
    return (this._value === undefined) ? <boolean> this.Static.defaultValue : this._value
  }

  public set value(value: boolean)
  {
    this._value = value
  }

  public process(): boolean
  {
    return <boolean> super.process()
  }
}