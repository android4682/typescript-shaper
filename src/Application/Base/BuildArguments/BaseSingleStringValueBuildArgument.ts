import { BaseStringValueBuildArgument } from "./BaseStringValueBuildArgument";

export abstract class BaseSingleStringValueBuildArgument extends BaseStringValueBuildArgument
{
  protected _value: string | undefined = undefined;
  public static readonly passingArguments: string[] = ['<string>'];
  
  public constructor(value?: string | undefined)
  {
    super(value)
    if (this.Static.passingArguments.length > 1) throw new Error(`A child class that extends '${this.Static.name}' can't have more then 1 passing argument.`)
  }

  public get value(): string
  {
    return <string> super.value
  }

  public set value(value: string)
  {
    this._value = value
  }

  public process(): string
  {
    let value = super.process()

    if (Array.isArray(value)) throw new Error('Single string build argument is an array.')

    return value 
  }
}