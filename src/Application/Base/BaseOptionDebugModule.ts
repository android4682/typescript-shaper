import { IBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { IOptionDebugModule } from "../../Domain/Interfaces/OptionDebugModule";

export class BaseOptionDebugModule implements IOptionDebugModule
{
  public readonly key: string;
  public readonly value: any;

  public constructor(key: string, value: any)
  {
    this.key = key
    this.value = value
  }

  public static fromBuildArgument(buildArgument: IBuilderArgument): IOptionDebugModule
  {
    return new BaseOptionDebugModule(buildArgument.Static.argumentSelector.join(', '), buildArgument.value)
  }

  public static fromBatchBuildArgument(buildArguments: IBuilderArgument[]): IOptionDebugModule[]
  {
    let result: IOptionDebugModule[] = []
    for (let i = 0; i < buildArguments.length; i++) result.push(BaseOptionDebugModule.fromBuildArgument(buildArguments[i]))
    
    return result
  }
}