import { IBuildProcessResult, TBuildProcessResultTypes } from "../../Domain/Interfaces/BuildProcess";

export class BaseBuildProcessResult<RESULT_TYPE> implements IBuildProcessResult<RESULT_TYPE>
{
  public readonly type: TBuildProcessResultTypes;
  public readonly results: RESULT_TYPE;

  public constructor(type: TBuildProcessResultTypes, results: RESULT_TYPE)
  {
    this.type = type
    this.results = results
  }
}