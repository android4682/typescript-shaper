import { IBuilderArgument, IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { IHelpBuildArgumentModule } from "../../Domain/Interfaces/HelpModules";

export class BaseHelpBuildArgumentModule implements IHelpBuildArgumentModule
{
  private _buildArgument: IBuilderArgument
  private _requiredBy: string[]
  private _usedBy: string[];

  public constructor(buildArgument: IStaticBuilderArgument, usedBy: string[] = [], requiredBy: string[] = [])
  {
    this._buildArgument = new buildArgument()
    this._buildArgument.process()
    this._requiredBy = requiredBy
    this._usedBy = usedBy
  }

  public get buildArgument(): IBuilderArgument
  {
    return this._buildArgument
  }

  public get argumentSelector(): string[]
  {
    return this._buildArgument.Static.argumentSelector
  }

  public get passingArguments(): string[]
  {
    return this._buildArgument.Static.passingArguments
  }

  public get description(): string[]
  {
    return this._buildArgument.Static.helpDescription
  }

  public get defaultValue(): string
  {
    let defaultValue: string
    switch (typeof this._buildArgument.Static.defaultValue) {
      case 'boolean':
        defaultValue = (this._buildArgument.Static.defaultValue) ? 'true' : 'false'
        break;
      case 'object':
        defaultValue = JSON.stringify(this._buildArgument.Static.defaultValue)
        break;
      case 'string':
        defaultValue = `'${this._buildArgument.Static.defaultValue}'`
        break;
    }
    return defaultValue
  }

  public get isRequired(): boolean
  {
    return (this._requiredBy.length > 0)
  }

  public get requiredBy(): string[]
  {
    return this._requiredBy
  }

  public get usedBy(): string[]
  {
    return this._usedBy
  }

  public isUsedBy(processName: string): boolean
  {
    return (this._usedBy.indexOf(processName) !== -1)
  }

  public addRequiredBy(processName: string): this
  {
    if (this._requiredBy.indexOf(processName) !== -1) return this

    this._requiredBy.push(processName)
    
    return this
  }

  public addUsedBy(processName: string): this
  {
    if (this._usedBy.indexOf(processName) !== -1) return this

    this._usedBy.push(processName)
    
    return this
  }

  public static fromBuildArgument(buildArgument: IStaticBuilderArgument): IHelpBuildArgumentModule
  {
    if (buildArgument.argumentSelector.length === 0) throw new Error('Given build argument doesn\'t have a argument selector.')
    
    return new BaseHelpBuildArgumentModule(buildArgument)
  }

  public static fromBatchBuildArgument(buildArguments: IBuilderArgument[], ignoreMissingArgumentSelector: boolean = false): IHelpBuildArgumentModule[]
  {
    let result: IHelpBuildArgumentModule[] = []
    for (let i = 0; i < buildArguments.length; i++) {
      const buildArgument = buildArguments[i];
      try {
        result.push(BaseHelpBuildArgumentModule.fromBuildArgument(buildArgument.Static))
      } catch (e) {
        let err = <Error> e
        if (err.message === 'Given build argument doesn\'t have a argument selector.' && ignoreMissingArgumentSelector) continue
        throw e
      }
    }
    
    return result
  }
}