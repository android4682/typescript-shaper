import { IStaticBuilderArgument } from "../../../Domain/Interfaces/BuilderArgument";
import { BuildPathArgument } from "../../Arguments/Builder/BuildPathArgument";
import { DistFileNameArgument } from "../../Arguments/Builder/DistFileNameArgument";
import { DistFolderArgument } from "../../Arguments/Builder/DistFolderArgument";
import { SubFolderArgument } from "../../Arguments/Builder/SubfolderArguments";
import { AbstractBuildProcess } from "./AbstractBuildProcess";

export abstract class AbstractCompiler extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    SubFolderArgument,
    DistFolderArgument,
    DistFileNameArgument,
    BuildPathArgument
  ]
}