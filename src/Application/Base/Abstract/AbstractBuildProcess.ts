import { StaticImplements } from "@android4682/typescript-toolkit";
import { IBuilderArgument, IStaticBuilderArgument } from "../../../Domain/Interfaces/BuilderArgument";
import { EBuildProcessResultTypes, IBuildProcess, IBuildProcessResult, IStaticBuildProcess, TBuildProcessRequiredResult, TBuildProcessResults } from "../../../Domain/Interfaces/BuildProcess";
import { MissingArgumentsException } from "../../Exceptions/MissingArgumentsException";
import { BaseBuildProcess } from "../BaseBuildProcess";
import { BuildStepManager } from "../../Classes/BuildStepManager";

export abstract class AbstractBuildProcess implements IBuildProcess, StaticImplements<IStaticBuildProcess, typeof BaseBuildProcess>
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = []
  public static readonly optionalArguments: IStaticBuilderArgument[] = []
  public static readonly requiredResults: TBuildProcessRequiredResult[] = []
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.VOID

  protected get Static(): typeof AbstractBuildProcess
  {
    return (this.constructor as typeof AbstractBuildProcess)
  }

  protected getArgument(argument: IStaticBuilderArgument): IBuilderArgument
  {
    const newArgument = new argument()
    newArgument.process()
    return newArgument
  }

  protected getResult(stepResults: TBuildProcessResults, type: EBuildProcessResultTypes): IBuildProcessResult<any>
  {
    let result: IBuildProcessResult<any> | undefined = undefined

    for (let i = 0; i < this.Static.requiredResults.length; i++) {
      const requiredResult = this.Static.requiredResults[i];
      if (requiredResult[1] === type) {
        for (let j = 0; j < requiredResult[0].length; j++) {
          const resultName = requiredResult[0][j];
          result = stepResults.get(resultName)
          if (result) break
        }
      }
    }

    if (result) return result

    stepResults.forEach((buildProcessResult, name) => {
      if (type === buildProcessResult.type) {
        result = buildProcessResult
        return false
      }
    })

    if (result) return result
    else throw new Error('Result not found.')
  }

  protected checkStepResultAvailability(stepResults: TBuildProcessResults): void
  {
    for (let i = 0; i < this.Static.requiredResults.length; i++) {
      const requiredResult = this.Static.requiredResults[i];
      try {
        BuildStepManager.getAnyValidStepResult(stepResults, requiredResult[0], requiredResult[1])
      } catch (e) {
        let err = <Error> e
        if (err.message.includes('Step not found')) throw new MissingArgumentsException(`Required step result #${i} of ${this.Static.name} with fallback type ${requiredResult[1]}`)
      }
    }
  }

  /**
   * @throws {MissingArgumentsException} stepResults param needs all arguments specified in the static requiredResults property
   */
  public process(stepResults: TBuildProcessResults): any
  {
    this.checkStepResultAvailability(stepResults)
  }
}