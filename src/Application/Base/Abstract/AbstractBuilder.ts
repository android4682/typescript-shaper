import { SpecialMap } from "@android4682/typescript-toolkit";
import { IBuilder } from "../../../Domain/Interfaces/Builder";
import { IBuilderArgument } from "../../../Domain/Interfaces/BuilderArgument";
import { IBuildProcessResult, TBuildProcessResults } from "../../../Domain/Interfaces/BuildProcess";
import { BuildStepManager } from "../../Classes/BuildStepManager";
import { HelpModuleManager } from "../../Classes/HelpModuleManager";
import { OptionDebugModuleManager } from "../../Classes/OptionDebugModuleManager";
import { BaseBuildProcessResult } from "../BaseBuildProcessResult";
import { BaseHelpBuildArgumentModule } from "../BaseHelpModule";
import { BaseOptionDebugModule } from "../BaseOptionDebugModule";
import { IHelpBuildArgumentModule } from "../../../Domain/Interfaces/HelpModules";
import { ProgramInfoHelper } from "../../Classes/ProgramInfoHelper";
import { BetterConsole, LogLevel } from "@android4682/better-console";

export enum EBuildArgumentType
{
  'REQUIRED',
  'OPTIONAL',
  'ALL'
}

export abstract class AbstractBuilder implements IBuilder
{
  public static readonly builder_name: string = 'BaseBuilder'
  
  public static readonly pre_build_steps: BuildStepManager = new BuildStepManager([])

  public static readonly build_steps: BuildStepManager = new BuildStepManager([])
  
  public static readonly compiler_steps: BuildStepManager = new BuildStepManager([]);
  
  public static readonly post_build_steps: BuildStepManager = new BuildStepManager([])

  public static getBuildArguments(type: EBuildArgumentType = EBuildArgumentType.ALL): IBuilderArgument[]
  {
    const build_arguments: IBuilderArgument[] = []
    this.help_build_arguments.forEach((helpBuildArgumentModule, argumentName) => {
      if (
        (type === EBuildArgumentType.REQUIRED && helpBuildArgumentModule.isRequired)
        || (type === EBuildArgumentType.OPTIONAL && ! helpBuildArgumentModule.isRequired)
        || (type === EBuildArgumentType.ALL)
      ) {
        build_arguments.push(helpBuildArgumentModule.buildArgument)
      }
    })
    return build_arguments
  }

  private static stepProcess(map: SpecialMap<string, IHelpBuildArgumentModule>, stepManager: BuildStepManager): SpecialMap<string, IHelpBuildArgumentModule>
  {
    stepManager.forEach((buildProcess, type, stepNumber) => {
      for (let i = 0; i < buildProcess.requiredArguments.length; i++) {
        const argument = buildProcess.requiredArguments[i];
        const helpModule = map.get(argument.name) || new BaseHelpBuildArgumentModule(argument)
        helpModule.addRequiredBy(buildProcess.name)
        helpModule.addUsedBy(buildProcess.name)
        map.set(argument.name, helpModule)
      }
  
      for (let i = 0; i < buildProcess.optionalArguments.length; i++) {
        const argument = buildProcess.optionalArguments[i];
        const helpModule = map.get(argument.name) || new BaseHelpBuildArgumentModule(argument)
        helpModule.addUsedBy(buildProcess.name)
        map.set(argument.name, helpModule)
      }
    })

    return map
  }

  public static get help_build_arguments(): SpecialMap<string, IHelpBuildArgumentModule>
  {
    let map = new SpecialMap<string, IHelpBuildArgumentModule>()

    map = this.stepProcess(map, this.pre_build_steps)
    map = this.stepProcess(map, this.build_steps)
    map = this.stepProcess(map, this.compiler_steps)
    map = this.stepProcess(map, this.post_build_steps)

    return map
  }
  
  protected static getUsageArgumentText(): string
  {
    let result = ''
    this.help_build_arguments.forEach((helpBuildArgumentModule, argumentName) => {
      if (helpBuildArgumentModule.argumentSelector.length === 0 || ! helpBuildArgumentModule.isRequired) return true
      result += `${helpBuildArgumentModule.argumentSelector[0]}${(helpBuildArgumentModule.passingArguments.length > 0) ? ' ' + helpBuildArgumentModule.passingArguments.join(' ') : ''} `
    })

    return result
  }

  protected static getUsageContentText(): string
  {
    return `${ProgramInfoHelper.getShortRelativeBinRunner()} -b ${this.builder_name}`
  }

  protected static generateUsageText(): string
  {
    return `Usage: ${this.getUsageContentText()} ${this.getUsageArgumentText()}\n\n`
  }

  public static getHelpText(): string
  {
    const helpModules: IHelpBuildArgumentModule[] = this.help_build_arguments.valuesAsArray()
    return this.generateUsageText() + HelpModuleManager.formatModules(helpModules)
  }

  public static getOptionsText(): string
  {
    return OptionDebugModuleManager.formatModules(BaseOptionDebugModule.fromBatchBuildArgument(this.getBuildArguments(EBuildArgumentType.ALL)))
  }
  
  protected get Static(): typeof AbstractBuilder
  {
    return (this.constructor as typeof AbstractBuilder)
  }

  protected shouldRunStep(currentStep: number, input: number | number[] | undefined): boolean
  {
    if (input === undefined) return true
    else if (Array.isArray(input)) return (input.indexOf(currentStep) !== -1)
    else return (currentStep === input)
  }

  protected async runGivenSteps(steps: BuildStepManager, stepResults: TBuildProcessResults, runStep: number | number[] | undefined = undefined): Promise<TBuildProcessResults>
  {
    await steps.forEachAsync(async (buildProcess, type, stepNumber) => {
      if (this.shouldRunStep(stepNumber, runStep)) {
        BetterConsole.Create().dev(`Step results at step '${buildProcess.name}'`)
        BetterConsole.Create().dir(stepResults, undefined, LogLevel.dev)
        stepResults.set(buildProcess.name, new BaseBuildProcessResult(type, await new buildProcess().process(stepResults)))
      }
    })

    return stepResults
  }

  protected async preBuild(stepResults: TBuildProcessResults, runStep: number | number[] | undefined = undefined): Promise<TBuildProcessResults>
  {
    return this.runGivenSteps(this.Static.pre_build_steps, stepResults, runStep)
  }

  protected build(stepResults: TBuildProcessResults, runStep: number | number[] | undefined = undefined): Promise<TBuildProcessResults>
  {
    return this.runGivenSteps(this.Static.build_steps, stepResults, runStep)
  }

  protected compile(stepResults: TBuildProcessResults, runStep: number | number[] | undefined = undefined): Promise<TBuildProcessResults>
  {
    return this.runGivenSteps(this.Static.compiler_steps, stepResults, runStep)
  }

  protected async postBuild(stepResults: TBuildProcessResults, runStep: number | number[] | undefined = undefined): Promise<TBuildProcessResults>
  {
    return this.runGivenSteps(this.Static.post_build_steps, stepResults, runStep)
  }

  public async runBuild(): Promise<void>
  {
    let stepResults = new SpecialMap<string, IBuildProcessResult<any>>()
    stepResults = await this.preBuild(stepResults)
    stepResults = await this.build(stepResults)
    stepResults = await this.compile(stepResults)
    stepResults = await this.postBuild(stepResults)
  }
}
