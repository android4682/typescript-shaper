import { StaticImplements } from "@android4682/typescript-toolkit";
import { IBuilderArgument, IStaticBuilderArgument } from "../../../Domain/Interfaces/BuilderArgument";
import { BaseBuildArgument } from "../BaseBuildArgument";

export abstract class AbstractBuildArgument implements IBuilderArgument, StaticImplements<IStaticBuilderArgument, typeof BaseBuildArgument>
{
  protected _value: string | boolean | string[] | undefined;
  public static readonly defaultValue: string | string[] | boolean = false;
  public static readonly argumentSelector: string[] = [];
  public static readonly passingArguments: string[] = []
  public static readonly helpDescription: string[] = ['<No help description available for this argument>'];

  public constructor(value?: any)
  {
    this._value = value
  }

  public get Static(): IStaticBuilderArgument
  {
    return (this.constructor as IStaticBuilderArgument)
  }

  public get value(): string | boolean | string[]
  {
    return this._value || this.Static.defaultValue
  }

  public set value(value: string | boolean | string[])
  {
    this._value = value
  }

  public static findArgumentSelectorIndex(): number
  {
    if (this.argumentSelector.length !== 0) for (let i = 0; i < this.argumentSelector.length; i++) {
      const argumentSelector = this.argumentSelector[i];
      let index = process.argv.indexOf(argumentSelector)
      if (index !== -1) return index
    }

    return -1
  }

  public process(): string | boolean | string[]
  {
    if (this.Static.argumentSelector.length === 0) return this.value
    let index = this.Static.findArgumentSelectorIndex()
    if (index === -1) return this.value
    switch (this.Static.passingArguments.length) {
      case 0:
        this._value = true
        break;
      case 1:
        this._value = process.argv[index+1]
        break;
      default:
        this._value = []
        for (let i = 1; i < this.Static.passingArguments.length + 1; i++) this._value.push(process.argv[index+i])
        break;
    }

    return this.value
  }
}
