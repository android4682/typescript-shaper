import { ICompilerResults } from "../../Domain/Interfaces/CompilerResults";

export class BaseCompilerResult implements ICompilerResults
{
  public jsFiles: string[];
  public sourceMapFiles: string[];
  public declarationFiles: string[];
  
  public constructor(jsFiles: string[] = [], declarationFiles: string[] = [], sourceMapFiles: string[] = [])
  {
    this.jsFiles = jsFiles
    this.sourceMapFiles = sourceMapFiles
    this.declarationFiles = declarationFiles
  }
}