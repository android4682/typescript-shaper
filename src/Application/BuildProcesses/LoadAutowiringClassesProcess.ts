import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import { SourceFolderArgument } from "../Arguments/Builder/SourceFolderArgument";
import fs from "node:fs"
import path from "node:path"
import { EBuildProcessResultTypes, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { AutowireConfigPathArgument } from "../Arguments/Builder/AutowireConfigPathArgument";
import { AutowireConfig } from "../../Domain/Interfaces/AutowireConfig";
import { AutowireCategories } from "../../Domain/Interfaces/AutowireCategories";
import { tryConvertToJSON } from "@android4682/typescript-toolkit";
import { BetterConsole, LogLevel } from "@android4682/better-console";
import { getClassNames } from "../Tools/Utils";

export class LoadAutowiringClassesProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    SourceFolderArgument,
    AutowireConfigPathArgument
  ];
  
  public static readonly optionalArguments: IStaticBuilderArgument[] = []

  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.AUTOWIRE_CLASS_NAMES

  private getFilesFromAutowirePath(autowirePath: string): string[]
  {
    const result: string[] = []
    if (fs.statSync(autowirePath).isDirectory()) {
      const files = fs.readdirSync(autowirePath)
      for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const filePath = path.join(autowirePath, file)

        result.push(...this.getFilesFromAutowirePath(filePath))
      }
    } else if (autowirePath.endsWith('.ts') && ! autowirePath.endsWith('.d.ts')) {
      result.push(autowirePath)
    }

    return result
  }

  private formatAutowirePath(autowirePath: string, sourceFolder: string): string
  {
    autowirePath = path.join(autowirePath.replaceAll('{{CWD}}', process.cwd()))
    return path.join(autowirePath.replaceAll('{{SRC}}', sourceFolder))
  }

  private async getAllClassNames(sourceFolder: string, autowireConfig: AutowireConfig): Promise<AutowireCategories>
  {
    const allClassNames: AutowireCategories = {}

    for (const category in autowireConfig) {
      if (Object.prototype.hasOwnProperty.call(autowireConfig, category)) {
        const autoWirePaths = autowireConfig[category];
        const filesInDirectory = [] 
        if (Array.isArray(autoWirePaths)) {
          for (let i = 0; i < autoWirePaths.length; i++) {
            const autoWirePath = autoWirePaths[i];
            const formattedAutowirePath = this.formatAutowirePath(autoWirePath, sourceFolder)
            
            filesInDirectory.push(...this.getFilesFromAutowirePath(formattedAutowirePath))
          }
        } else {
          const formattedAutowirePath = this.formatAutowirePath(autoWirePaths, sourceFolder)
            
          filesInDirectory.push(...this.getFilesFromAutowirePath(formattedAutowirePath))
        }
    
        for (let j = 0; j < filesInDirectory.length; j++) {
          const filePath = filesInDirectory[j];
          
          if (! allClassNames[category]) allClassNames[category] = []

          if (! filePath.endsWith('.ts') || filePath.endsWith('.d.ts')) continue
          allClassNames[category] = allClassNames[category].concat(getClassNames(filePath))
        }
      }
    }

    return allClassNames
  }

  public async process(stepResults: TBuildProcessResults): Promise<AutowireCategories>
  {
    super.process(stepResults)
    const sourceFolderArgument = <SourceFolderArgument> this.getArgument(SourceFolderArgument)
    const autowireConfigPathArgument = <AutowireConfigPathArgument> this.getArgument(AutowireConfigPathArgument)

    const autowireConfig = <AutowireConfig|string> tryConvertToJSON(fs.readFileSync(autowireConfigPathArgument.value).toString())
    let results: AutowireCategories = {}
    if (typeof autowireConfig === 'string') {
      return results
    }

    const logger = BetterConsole.Create()

    logger.info("Getting all autowire files...")
    results = await this.getAllClassNames(sourceFolderArgument.value, autowireConfig)

    logger.debug('Fetched class names:')
    logger.dir(results, undefined, LogLevel.debug)

    return results
  }
}
