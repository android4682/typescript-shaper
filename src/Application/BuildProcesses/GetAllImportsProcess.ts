import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import { EBuildProcessResultTypes, IBuildProcessResult, ImportData, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { JoinFilesProcess } from "./JoinFilesProcess";
import { BetterConsole } from "@android4682/better-console";

export class GetAllImportsProcess extends AbstractBuildProcess
{
  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[JoinFilesProcess.name], EBuildProcessResultTypes.FILE_CONTENT]
  ]

  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.IMPORTS

  private getAllImports(finalFileContent: string): ImportData
  {
    let allImportData = finalFileContent.matchAll(new RegExp(/^\s*import (?:|([{}a-zA-Z\s,*_]*) from )(?:"|')([A-Za-z@_\-0-9\/:]+)(?:"|');?\s*$/, 'gm'))
    let importDataRegexpResult = allImportData.next()

    const importData: ImportData = {
      static: {},
      dynamic: {}
    }

    while (! importDataRegexpResult.done) {
      const imports: string | undefined = importDataRegexpResult.value[1]
      const moduleName: string          = importDataRegexpResult.value[2]
      if (!! imports && imports.startsWith('{')) {
        const allDynamicImportData = imports.matchAll(new RegExp(/(\w+ as \w+|\w+)/, 'gm'))
        let dynamicImportDataRegexpResult = allDynamicImportData.next()
        while (! dynamicImportDataRegexpResult.done) {
          const dynamicImports = dynamicImportDataRegexpResult.value[1]
          if (! Array.isArray(importData.dynamic[moduleName])) importData.dynamic[moduleName] = []
          if (importData.dynamic[moduleName].indexOf(dynamicImports) === -1) importData.dynamic[moduleName].push(dynamicImports)
          dynamicImportDataRegexpResult = allDynamicImportData.next()
        }
      } else {
        importData.static[moduleName] = imports
      }
      importDataRegexpResult = allImportData.next()
    }

    return importData
  }

  public process(stepResults: TBuildProcessResults): ImportData
  {
    super.process(stepResults)
    const fileContent = <IBuildProcessResult<string>> this.getResult(stepResults, EBuildProcessResultTypes.FILE_CONTENT)

    BetterConsole.Create().info("Finding all imports...")
    return this.getAllImports(fileContent.results)
  }
}