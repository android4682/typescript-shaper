import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractCompiler } from "../Base/Abstract/AbstractCompiler";
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { BetterConsole } from "@android4682/better-console";
import { LoadAutowiringClassesProcess } from "./LoadAutowiringClassesProcess";
import { AutowireCategories } from "../../Domain/Interfaces/AutowireCategories";
import fs from "node:fs"
import { DistFileNameArgument } from "../Arguments/Builder/DistFileNameArgument";
import { DistFolderArgument } from "../Arguments/Builder/DistFolderArgument";
import path from "node:path";

export class CreatePreCompiledAutoWireMappingProcess extends AbstractCompiler
{
  public static readonly optionalArguments: IStaticBuilderArgument[] = [
    DistFolderArgument,
    DistFileNameArgument
  ];
  
  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[LoadAutowiringClassesProcess.name], EBuildProcessResultTypes.AUTOWIRE_CLASS_NAMES]
  ];
  
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.VOID

  public process(stepResults: TBuildProcessResults): void
  {
    super.process(stepResults)
    const distFolderArgument = <DistFolderArgument> this.getArgument(DistFolderArgument)
    const distFileNameArgument = <DistFileNameArgument> this.getArgument(DistFileNameArgument)

    const autowireCategoriesResults = <IBuildProcessResult<AutowireCategories>> this.getResult(stepResults, EBuildProcessResultTypes.AUTOWIRE_CLASS_NAMES)
    
    const autowireCategories = autowireCategoriesResults.results

    BetterConsole.Create().info("Generating pre-compiled autowire categories...")

    const distFilePath = path.join(distFolderArgument.value, distFileNameArgument.value)
    const fileContent = fs.readFileSync(distFilePath).toString()
    fs.writeFileSync(distFilePath, `const dynamic_classes = ${JSON.stringify(autowireCategories)};\n${fileContent}`)
  }
}
