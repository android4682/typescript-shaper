import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import fs from 'node:fs';
import path from 'node:path';
import { ICompilerResults } from "../../Domain/Interfaces/CompilerResults";
import { DistFolderArgument } from "../Arguments/Builder/DistFolderArgument";
import { DistFileNameArgument } from "../Arguments/Builder/DistFileNameArgument";
import { TypesPathArgument } from "../Arguments/Builder/TypesPathArgument";
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { NodeTypeScriptCompiler } from "../Compilers/NodeTypeScriptCompiler";
import { BetterConsole } from "@android4682/better-console";
import { inspect } from "node:util";

export class MoveCompiledFilesToDistProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    DistFolderArgument
  ];
  
  public static readonly optionalArguments: IStaticBuilderArgument[] = [
    DistFileNameArgument,
    TypesPathArgument
  ];

  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[NodeTypeScriptCompiler.name], EBuildProcessResultTypes.COMPILER_RESULTS]
  ];

  public process(stepResults: TBuildProcessResults): void
  {
    super.process(stepResults)
    let betterConsole = BetterConsole.Create()
    const distFolderArgument = <DistFolderArgument> this.getArgument(DistFolderArgument)
    const distFileNameArgument = <DistFileNameArgument> this.getArgument(DistFileNameArgument)
    const typesPathArgument = <TypesPathArgument> this.getArgument(TypesPathArgument)
    const compilerResults = <IBuildProcessResult<ICompilerResults>> this.getResult(stepResults, EBuildProcessResultTypes.COMPILER_RESULTS)
    
    if (compilerResults.results.jsFiles.length > 0) {
      betterConsole.info("Moving compiled 'JavaScript' files to dist folder...")
      if (compilerResults.results.jsFiles.length === 1) {
        fs.copyFileSync(compilerResults.results.jsFiles[0], path.join(distFolderArgument.value, distFileNameArgument.value))
      } else {
        for (let i = 0; i < compilerResults.results.jsFiles.length; i++) {
          const file = compilerResults.results.jsFiles[i];
          fs.copyFileSync(file, path.join(distFolderArgument.value, path.basename(file)))
        }
      }
    }
    
    if (compilerResults.results.sourceMapFiles.length > 0) {
      betterConsole.info("Moving compiled 'JavaScript Sourcemap' files to dist folder...")
      if (compilerResults.results.sourceMapFiles.length === 1) {
        fs.copyFileSync(compilerResults.results.sourceMapFiles[0], path.join(distFolderArgument.value, path.basename(distFileNameArgument.value, '.js') + '.js.map'))
      } else {
        for (let i = 0; i < compilerResults.results.sourceMapFiles.length; i++) {
          const file = compilerResults.results.sourceMapFiles[i];
          fs.copyFileSync(file, path.join(distFolderArgument.value, path.basename(file)))
        }
      }
    }
    
    if (compilerResults.results.declarationFiles.length > 0) {
      betterConsole.info("Moving compiled 'Typescript Declaration' files to dist folder...")
      let declarationPath = typesPathArgument.value
      if (compilerResults.results.declarationFiles.length === 1) {
        fs.copyFileSync(compilerResults.results.declarationFiles[0], path.join(declarationPath, path.basename(distFileNameArgument.value, '.js') + '.d.ts'))
      } else {
        for (let i = 0; i < compilerResults.results.declarationFiles.length; i++) {
          const file = compilerResults.results.declarationFiles[i];
          fs.copyFileSync(file, path.join(declarationPath, path.basename(file)))
        }
      }
    }
  }
}