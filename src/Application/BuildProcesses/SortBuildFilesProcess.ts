import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import { SourceFolderArgument } from "../Arguments/Builder/SourceFolderArgument";
import fs from "node:fs"
import { BuildOrderFileArgument } from "../Arguments/Global/BuildOrderFileArgument";
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { GetAllFilesProcess } from "./GetAllFilesProcess";
import { BetterConsole, LogLevel } from "@android4682/better-console";
import { BuildFileSorter, SortableBuildFile } from "../Classes/BuildFileSorter";
import { getClassNames, getExportedConsts, getImportsFromNonModules, getInterfaceNames, getLooseFunctionNames, getTypesNames } from "../Tools/Utils";
import { BuildOrder } from "../../Domain/Interfaces/BuildOrder";
import path from "node:path";

export class SortBuildFilesProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    SourceFolderArgument,
    BuildOrderFileArgument
  ];

  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[GetAllFilesProcess.name], EBuildProcessResultTypes.FILE_LIST]
  ];

  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.FILE_LIST

  protected async generateFileOrder(files: string[], buildOrder?: BuildOrder, sorter: BuildFileSorter = new BuildFileSorter()): Promise<string[]>
  {
    const betterConsole = BetterConsole.Create()

    for (let i = 0; i < files.length; i++) {
      const relativeFilePath = files[i];
      const interfaces = getInterfaceNames(relativeFilePath)
      const classes = getClassNames(relativeFilePath)
      const types = getTypesNames(relativeFilePath)
      const dependsOn = getImportsFromNonModules(relativeFilePath)
      const functions = getLooseFunctionNames(relativeFilePath)
      const exportedConst = getExportedConsts(relativeFilePath)
      
      const obj: SortableBuildFile = {
        includes: [...interfaces, ...classes, ...types, ...functions, ...exportedConst],
        dependsOn,
        filePath: relativeFilePath,
      }
      
      if (buildOrder && Object.keys(buildOrder).includes(relativeFilePath)) {
        obj.sorter = buildOrder[relativeFilePath]
      }

      sorter.add(obj)
    }

    betterConsole.dir({items: sorter.items}, {
      depth: 5
    }, LogLevel.dev)

    const items = sorter.sort()

    betterConsole.dir({sortedItems: items}, {
      depth: 5
    }, LogLevel.dev)

    return sorter.getArray<string>('filePath', items)
  }

  public async process(stepResults: TBuildProcessResults): Promise<string[]>
  {
    super.process(stepResults)
    const betterConsole = BetterConsole.Create()
    const buildOrderFileArgument = <BuildOrderFileArgument> this.getArgument(BuildOrderFileArgument)
    const files = <IBuildProcessResult<string[]>> this.getResult(stepResults, EBuildProcessResultTypes.FILE_LIST)

    let buildOrder: BuildOrder | undefined = undefined
    if (fs.existsSync(buildOrderFileArgument.value)) {
      const imp = <{default: BuildOrder}> await import(path.join(process.cwd(), buildOrderFileArgument.value))
      buildOrder = imp.default
    }

    betterConsole.info('Sorting files...')
    const sortedFileOrder: string[] = await this.generateFileOrder(files.results, buildOrder)

    betterConsole.dir(sortedFileOrder, undefined, LogLevel.debug)

    return sortedFileOrder
  }
}
