import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import fs from 'node:fs';
import path from 'node:path';
import { BuildPathArgument } from "../Arguments/Builder/BuildPathArgument";
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { JoinFilesProcess } from "./JoinFilesProcess";
import { BetterConsole } from "@android4682/better-console";
import { PrepandImportsProcess } from "./PrepandImportsProcess";

export class BuildProcessedFileContentProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    BuildPathArgument
  ];

  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.BUILD_FILE_PATH

  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[PrepandImportsProcess.name, JoinFilesProcess.name], EBuildProcessResultTypes.FILE_CONTENT]
  ]

  public process(stepResults: TBuildProcessResults): string
  {
    super.process(stepResults)
    const buildPathArgument = <BuildPathArgument> this.getArgument(BuildPathArgument)
    const fileContent = <IBuildProcessResult<string>> this.getResult(stepResults, EBuildProcessResultTypes.FILE_CONTENT)

    BetterConsole.Create().info('Preparing build directory...')
    let outputPath = path.join(buildPathArgument.value, 'joined.ts')

    if (! fs.existsSync(buildPathArgument.value)) fs.mkdirSync(buildPathArgument.value, { recursive: true })
    fs.writeFileSync(outputPath, fileContent.results)

    if (! fs.existsSync(outputPath)) throw new Error(`Failed to write file contents to temporary build file at ${outputPath}.`)
    else return outputPath
  }
}