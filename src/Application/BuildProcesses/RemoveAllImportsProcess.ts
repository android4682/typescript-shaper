import { BetterConsole } from "@android4682/better-console";
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import { JoinFilesProcess } from "./JoinFilesProcess";

export class RemoveAllImportsProcess extends AbstractBuildProcess
{
  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[JoinFilesProcess.name], EBuildProcessResultTypes.FILE_CONTENT]
  ];
  
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.FILE_CONTENT

  private removeAllImports(fileContent: string): string
  {
    return fileContent.replaceAll(new RegExp(/^\s*import.*$\s*/, 'gm'), '')
  }

  public process(stepResults: TBuildProcessResults): string
  {
    super.process(stepResults)
    const fileContent = <IBuildProcessResult<string>> this.getResult(stepResults, EBuildProcessResultTypes.FILE_CONTENT)

    BetterConsole.Create().info("Removing all imports from joined file...")
    return this.removeAllImports(fileContent.results)
  }
}