import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import fs from "node:fs"
import { KeepExportsArgument } from "../Arguments/Builder/KeepExportsArgument";
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { SortBuildFilesProcess } from "./SortBuildFilesProcess";
import { GetAllFilesProcess } from "./GetAllFilesProcess";
import { BetterConsole, LogLevel } from "@android4682/better-console";

export class JoinFilesProcess extends AbstractBuildProcess
{
  public static readonly optionalArguments: IStaticBuilderArgument[] = [
    KeepExportsArgument
  ];

  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[SortBuildFilesProcess.name, GetAllFilesProcess.name], EBuildProcessResultTypes.FILE_LIST]
  ];
  
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.FILE_CONTENT

  private joinFile(filepath: string, keepExports: boolean): string
  {
    let betterConsole = BetterConsole.Create()
    if (! fs.existsSync(filepath)) throw new Error('File not found. ' + filepath)
    let content: string = fs.readFileSync(filepath).toString().replaceAll(new RegExp(/^\s*import .+"\..+(?:;|)$\s*/, 'gm'), "")
    if (! keepExports) content = content.replaceAll(new RegExp(/^\s*export (?:default .*$|\{.*|\s*)/, 'gm'), "")
    betterConsole.debug(`Joining file '${filepath}'`)
    betterConsole.dir(content, undefined, LogLevel.dev)
    content += "\n\n"
    return content
  }

  public process(stepResults: TBuildProcessResults): string
  {
    super.process(stepResults)
    let betterConsole = BetterConsole.Create()
    const keepExportsArgument = <KeepExportsArgument> this.getArgument(KeepExportsArgument)
    const files = <IBuildProcessResult<string[]>> this.getResult(stepResults, EBuildProcessResultTypes.FILE_LIST)
    
    betterConsole.info("Joining all files...")
    betterConsole.dev('File list:')
    betterConsole.dir(files, undefined, LogLevel.dev)
    let finalFileContent = ''
    for (let i = 0; i < files.results.length; i++) {
      const filepath = files.results[i];
      finalFileContent += this.joinFile(filepath, keepExportsArgument.value)
    }

    return finalFileContent
  }
}