import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import fs from 'node:fs';
import { DistFolderArgument } from "../Arguments/Builder/DistFolderArgument";
import { BuildPathArgument } from "../Arguments/Builder/BuildPathArgument";
import { TypesPathArgument } from "../Arguments/Builder/TypesPathArgument";
import { EnableTypeBuildingArgument } from "../Arguments/Builder/EnableTypeBuildingArgument";
import { EBuildProcessResultTypes, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { BetterConsole } from "@android4682/better-console";

export class PreparePathsProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    DistFolderArgument,
    BuildPathArgument
  ];

  public static readonly optionalArguments: IStaticBuilderArgument[] = [
    TypesPathArgument,
    EnableTypeBuildingArgument
  ];

  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.VOID

  public process(stepResults: TBuildProcessResults): void
  {
    super.process(stepResults)
    let betterConsole = BetterConsole.Create()
    const buildPathArgument = <BuildPathArgument> this.getArgument(BuildPathArgument)
    const distFolderArgument = <DistFolderArgument> this.getArgument(DistFolderArgument)
    const typesPathArgument = <TypesPathArgument> this.getArgument(TypesPathArgument)
    const enableTypeBuildingArgument = <EnableTypeBuildingArgument> this.getArgument(EnableTypeBuildingArgument)
    
    betterConsole.info("Preparing dist directory...")
    if (! fs.existsSync(distFolderArgument.value)) fs.mkdirSync(distFolderArgument.value, {recursive: true})
    betterConsole.info("Preparing build directory...")
    if (! fs.existsSync(buildPathArgument.value)) fs.mkdirSync(buildPathArgument.value, {recursive: true})
    if (enableTypeBuildingArgument.value) {
      betterConsole.info("Preparing types directory...")
      if (! fs.existsSync(typesPathArgument.value)) fs.mkdirSync(typesPathArgument.value, {recursive: true})
    }
  }
}