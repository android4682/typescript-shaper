import { BetterConsole } from "@android4682/better-console";
import { EBuildProcessResultTypes, IBuildProcessResult, ImportData, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import { GetAllImportsProcess } from "./GetAllImportsProcess";
import { JoinFilesProcess } from "./JoinFilesProcess";
import { RemoveAllImportsProcess } from "./RemoveAllImportsProcess";

export class PrepandImportsProcess extends AbstractBuildProcess
{
  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[RemoveAllImportsProcess.name, JoinFilesProcess.name], EBuildProcessResultTypes.FILE_CONTENT],
    [[GetAllImportsProcess.name], EBuildProcessResultTypes.IMPORTS]
  ];
  
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.FILE_CONTENT

  private prepandImports(fileContent: string, importData: ImportData): string
  {
    let result = ''
    for (const moduleName in importData.static) {
      if (Object.prototype.hasOwnProperty.call(importData.static, moduleName)) {
        const imports = importData.static[moduleName];
        if (imports === undefined) result += `import "${moduleName}"\n`
        else result += `import ${imports} from "${moduleName}"\n`
      }
    }
    for (const moduleName in importData.dynamic) {
      if (Object.prototype.hasOwnProperty.call(importData.dynamic, moduleName)) {
        const imports = importData.dynamic[moduleName];
        result += `import { ${imports.join(", ")} } from "${moduleName}"\n`
      }
    }
    return `${result}\n${fileContent}`
  }

  public process(stepResults: TBuildProcessResults): string
  {
    super.process(stepResults)
    const fileContent = <IBuildProcessResult<string>> this.getResult(stepResults, EBuildProcessResultTypes.FILE_CONTENT)
    const importData = <IBuildProcessResult<ImportData>> this.getResult(stepResults, EBuildProcessResultTypes.IMPORTS)

    BetterConsole.Create().info("Prepanding imports...")
    return this.prepandImports(fileContent.results, importData.results)
  }
}