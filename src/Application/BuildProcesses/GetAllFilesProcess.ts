import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import { IgnoreDeclarationFilesInSourceFolderArgument } from "../Arguments/Builder/IgnoreDeclarationFilesInSourceFolderArgument";
import { SourceFolderArgument } from "../Arguments/Builder/SourceFolderArgument";
import fs from "node:fs"
import path from "node:path"
import { BuildPathArgument } from "../Arguments/Builder/BuildPathArgument";
import { EBuildProcessResultTypes, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { BetterConsole } from "@android4682/better-console";
import { TsconfigPathArgument } from "../Arguments/Compiler/tsconfigPathArgument";
import { TsConfig } from "../../Domain/Libraries/tsconfig";
import { fixGlob, globPromise } from "../Tools/Glob";

export class GetAllFilesProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    SourceFolderArgument,
    BuildPathArgument,
    TsconfigPathArgument
  ];
  
  public static readonly optionalArguments: IStaticBuilderArgument[] = [
    IgnoreDeclarationFilesInSourceFolderArgument
  ]

  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.FILE_LIST

  private processFile(filePath: string, buildPath: string, ignoreDeclarationFiles: boolean = IgnoreDeclarationFilesInSourceFolderArgument.defaultValue): boolean
  {
    let fileName = path.basename(filePath);

    if (fileName.endsWith('.d.ts') && ! ignoreDeclarationFiles) {
      fs.copyFileSync(filePath, path.join(buildPath, fileName))

      return false
    }

    return true
  }

  private async filterFilesExcludedByExcludedGlobPattern(excludedGlobPatterns: string[], files: string[]): Promise<string[]>
  {
    if (excludedGlobPatterns.length === 0) return files

    const excludedFiles: string[] = []
    for (let i = 0; i < excludedGlobPatterns.length; i++) excludedFiles.push(...await globPromise(excludedGlobPatterns[i]))

    const results: string[] = []
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      if (! excludedFiles.includes(file)) results.push(file)
    }

    return results
  }

  private async getAllFilesByGlobbing(sourceFolder: string, buildPath: string, ignoreDeclarationFiles: boolean = IgnoreDeclarationFilesInSourceFolderArgument.defaultValue, globPatterns: string[] = ['./**/*.ts'], excludedGlobPatterns: string[] = []): Promise<string[]>
  {
    let allFiles: string[] = []
    sourceFolder = sourceFolder.split(path.win32.sep).join(path.posix.sep)
    const fixedGlobPatterns = fixGlob(globPatterns, sourceFolder)
    const fixedExcludedGlobPatterns = fixGlob(excludedGlobPatterns, sourceFolder)

    for (let i = 0; i < fixedGlobPatterns.length; i++) {
      let files = await globPromise(fixedGlobPatterns[i])
      let filteredFiles = await this.filterFilesExcludedByExcludedGlobPattern(fixedExcludedGlobPatterns, files)

      for (let j = 0; j < filteredFiles.length; j++) {
        const filePath = filteredFiles[j];
        if (filePath.includes('node_modules')) continue
        if (this.processFile(filePath, buildPath, ignoreDeclarationFiles)) allFiles.push(filePath)
      }
    }

    return allFiles
  }

  private getAllFiles(sourceFolder: string, buildPath: string, ignoreDeclarationFiles: boolean = IgnoreDeclarationFilesInSourceFolderArgument.defaultValue, regex: RegExp = new RegExp(/.*\.ts$/)): string[]
  {
    let allFiles: string[] = []
    const files = fs.readdirSync(sourceFolder)
    for (let i = 0; i < files.length; i++) {
      const filename = files[i];
      const filepath = path.join(sourceFolder, filename)
      if (fs.lstatSync(filepath).isDirectory()) allFiles = allFiles.concat(this.getAllFiles(filepath, buildPath, ignoreDeclarationFiles, regex))
      else if (filename.endsWith('.d.ts') && ! ignoreDeclarationFiles) fs.copyFileSync(filepath, path.join(buildPath, filename))
      else if (filename.match(regex) !== null) allFiles.push(filepath)
    }
    return allFiles
  }

  public async process(stepResults: TBuildProcessResults): Promise<string[]>
  {
    super.process(stepResults)
    const sourceFolderArgument = <SourceFolderArgument> this.getArgument(SourceFolderArgument)
    const buildFolderArgument = <BuildPathArgument> this.getArgument(BuildPathArgument)
    const tsconfigPathArgument = <TsconfigPathArgument> this.getArgument(TsconfigPathArgument)
    const ignoreDeclarationFilesInSourceFolderArgument = <IgnoreDeclarationFilesInSourceFolderArgument> this.getArgument(IgnoreDeclarationFilesInSourceFolderArgument)

    let tsconfig: TsConfig
    let fileGlobs: string[] = []
    let excludeFiles: string[] = []
    try {
      tsconfig = JSON.parse(fs.readFileSync(tsconfigPathArgument.value).toString())

      let includes = <string[]|undefined> tsconfig.include
      if (Array.isArray(includes)) {
        fileGlobs = includes
      }
      let excludes = <string[]|undefined> tsconfig.exclude
      if (Array.isArray(excludes)) {
        excludeFiles = excludes
      }
    } catch (e) {
      let err: Error = <Error> e
      if (err.message.includes('Unexpected token')) throw new Error('tsconfig has invalid json or couldn\'t be found at ' + tsconfigPathArgument.value)
      throw err
    }

    BetterConsole.Create().info("Getting all files...")
    let results: string[] = []
    if (fileGlobs.length === 0) {
      results = this.getAllFiles(sourceFolderArgument.value, buildFolderArgument.value, ignoreDeclarationFilesInSourceFolderArgument.value)
    } else {
      results = await this.getAllFilesByGlobbing(sourceFolderArgument.value, buildFolderArgument.value, ignoreDeclarationFilesInSourceFolderArgument.value, fileGlobs, excludeFiles)
    }

    return results
  }
}