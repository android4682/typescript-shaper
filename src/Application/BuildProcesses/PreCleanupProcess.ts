import { deleteFile } from "@android4682/typescript-toolkit";
import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import fs from 'node:fs';
import { DistFolderArgument } from "../Arguments/Builder/DistFolderArgument";
import { SkipPrebuildCleanupArgument } from "../Arguments/Builder/SkipPrebuildCleanupArgument";
import { TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { BetterConsole } from "@android4682/better-console";

export class PreCleanupProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    SkipPrebuildCleanupArgument,
    DistFolderArgument
  ];

  public process(stepResults: TBuildProcessResults): void
  {
    super.process(stepResults)
    const skipPrebuildCleanupArgument = <SkipPrebuildCleanupArgument> this.getArgument(SkipPrebuildCleanupArgument)
    const distFolderArgument = <DistFolderArgument> this.getArgument(DistFolderArgument)
    
    if (! skipPrebuildCleanupArgument.value) {
      BetterConsole.Create().info("Cleaning target folders...")
      if (fs.existsSync(distFolderArgument.value)) deleteFile(distFolderArgument.value)
    }
  }
}