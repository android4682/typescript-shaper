import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import fs from "node:fs"
import { EBuildProcessResultTypes, IBuildProcessResult, TBuildProcessRequiredResult, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { BetterConsole } from "@android4682/better-console";
import { GetAllFilesProcess } from "./GetAllFilesProcess";
import { BuildOrderFileArgument } from "../Arguments/Global/BuildOrderFileArgument";
import { gitMergeArray } from "@android4682/typescript-toolkit";

export class GenerateBuildFileOrderProcess extends AbstractBuildProcess
{
  public static readonly optionalArguments: IStaticBuilderArgument[] = [
    BuildOrderFileArgument
  ];

  public static readonly requiredResults: TBuildProcessRequiredResult[] = [
    [[GetAllFilesProcess.name], EBuildProcessResultTypes.FILE_LIST]
  ];
  
  public static readonly returnType: EBuildProcessResultTypes = EBuildProcessResultTypes.BUILD_ORDER_FILE

  private generateFileOrder(files: string[], fileOrderJson: string): string[]
  {
    let betterConsole = BetterConsole.Create()
    betterConsole.info("Generating file order...")
  
    betterConsole.info("Getting all files...")
  
    if (fs.existsSync(fileOrderJson)) {
      betterConsole.info("Loading file order file...")
      let currentBuildOrder = JSON.parse(fs.readFileSync(fileOrderJson).toString())
      let mergedArray = <string[]> gitMergeArray(currentBuildOrder, files)
      fs.writeFileSync(fileOrderJson, JSON.stringify(mergedArray))
      return mergedArray
    } else {
      fs.writeFileSync(fileOrderJson, JSON.stringify(files))
      return files
    }
  }

  public process(stepResults: TBuildProcessResults): string[]
  {
    super.process(stepResults)
    const buildOrderFileArgument = <BuildOrderFileArgument> this.getArgument(BuildOrderFileArgument)
    const files = <IBuildProcessResult<string[]>> this.getResult(stepResults, EBuildProcessResultTypes.FILE_LIST)

    return this.generateFileOrder(files.results, buildOrderFileArgument.value)
  }
}