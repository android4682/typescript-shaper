import { deleteFile } from "@android4682/typescript-toolkit";
import { IStaticBuilderArgument } from "../../Domain/Interfaces/BuilderArgument";
import { AbstractBuildProcess } from "../Base/Abstract/AbstractBuildProcess";
import fs from 'node:fs';
import { BuildPathArgument } from "../Arguments/Builder/BuildPathArgument";
import { SkipCleanupArgument } from "../Arguments/Builder/SkipCleanup";
import { TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { BetterConsole } from "@android4682/better-console";

export class PostCleanupProcess extends AbstractBuildProcess
{
  public static readonly requiredArguments: IStaticBuilderArgument[] = [
    SkipCleanupArgument,
    BuildPathArgument
  ];

  public process(stepResults: TBuildProcessResults): void
  {
    super.process(stepResults)
    const skipCleanupArgument = <SkipCleanupArgument> this.getArgument(SkipCleanupArgument)
    const buildPathArgument = <BuildPathArgument> this.getArgument(BuildPathArgument)
    
    BetterConsole.Create().info('Cleaning up...')
    if (! skipCleanupArgument.value && fs.existsSync(buildPathArgument.value)) deleteFile(buildPathArgument.value)
  }
}