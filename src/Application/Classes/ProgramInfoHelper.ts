import path from "path";

export class ProgramInfoHelper
{
  public static getApplicationRunner(): string
  {
    return process.argv[0]
  }

  public static getApplicationRunnersName(): string
  {
    return path.basename(process.argv[0], '.exe')
  }

  public static getBinPath(): string
  {
    return process.argv[1]
  }

  public static getBinRelativePath(): string
  {
    return '.' + path.join(this.getBinPath().replace(process.cwd(), ''))
  }

  public static getBinName(): string
  {
    return path.basename(process.argv[1])
  }

  public static getBinRunner(): string
  {
    return `${this.getApplicationRunner()} ${this.getBinPath()}`
  }

  public static getShortBinRunner(): string
  {
    return `${this.getApplicationRunnersName()} ${this.getBinPath()}`
  }

  public static getShortRelativeBinRunner(): string
  {
    return `${this.getApplicationRunnersName()} ${this.getBinRelativePath()}`
  }

  public static getApplicationName(): string
  {
    return 'Typescript Shaper'
  }

  public static getVersion(): string
  {
    return require('../../../package.json').version
  }

  public static getHeader(): string
  {
    return `${this.getApplicationName()} - Custom TS compiler for all your TypeScript compiling needs!
Version: ${this.getVersion()}`
  }

  public static getFooter(): string
  {
    return `Credits: Andrew de Jong (Lead Developer)`
  }
}