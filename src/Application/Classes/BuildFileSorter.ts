import { BetterConsole } from "@android4682/better-console";
import { getClassNames, getImportsFromNonModules, getInterfaceNames, getLooseFunctionNames, getTypesNames } from "../Tools/Utils";
import { CustomSorterFunc } from "../../Domain/Interfaces/BuildOrder";

export interface SortableBuildFile
{
  filePath: string
  includes: string[]
  dependsOn: string[]
  customSorted?: boolean
  sorter?: CustomSorterFunc
  debug?: any[]
}

export class BuildFileSorter<C extends SortableBuildFile = SortableBuildFile>
{
  private logger: BetterConsole

  public constructor(private _items: C[] = [])
  {
    this.logger = BetterConsole.Create()
  }

  public get items(): C[]
  {
    return this._items
  }

  public add(nItem: C): this
  {
    this._items.push(nItem)

    return this
  }

  public addFromFile(filePath: string): this
  {
    const interfaces = getInterfaceNames(filePath)
    const classes = getClassNames(filePath)
    const types = getTypesNames(filePath)
    const dependsOn = getImportsFromNonModules(filePath)
    const functions = getLooseFunctionNames(filePath)
    
    this.add(<C> {
      includes: [...interfaces, ...classes, ...types, ...functions],
      dependsOn,
      filePath: filePath,
    })

    return this
  }

  public addBatchFiles(filePaths: string[]): this
  {
    for (let i = 0; i < filePaths.length; i++) {
      const filePath = filePaths[i];
      
      this.addFromFile(filePath)
    }

    return this
  }

  public getArray<V>(valueKey: keyof C, items: C[] = this._items): V[]
  {
    const results: V[] = []

    for (let i = 0; i < items.length; i++) results.push(<V> items[i][valueKey])

    return results
  }

  public findDependency(name: string, arr: C[] = this.items): [number, C] | undefined
  {
    for (let i = 0; i < arr.length; i++) {
      const item = arr[i];
      if (item.includes.includes(name)) return [i, item]
    }

    return undefined
  }

  protected doesArrayHaveAllDepedencies(item: C, sortedItems: C[]): boolean
  {
    for (let i = 0; i < item.dependsOn.length; i++) {
      const dependency = item.dependsOn[i];
      if (this.findDependency(dependency, sortedItems) === undefined) return false
    }

    return true
  }

  protected _sortingProcess(items: C[], sortedItems: C[]): [C[], C[]]
  {
    const processedIndexes: number[] = []

    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (item.dependsOn.length === 0 || this.doesArrayHaveAllDepedencies(item, sortedItems)) {
        sortedItems.push(item)
        processedIndexes.push(i)
      }
    }

    return [items.filter((_, index) => (! processedIndexes.includes(index))), sortedItems]
  }

  protected hasCirculairDependency(item: C, items: C[], previousDependencies: string[] = []): string[] | null
  {
    if (item.dependsOn.length === 0) return null
    for (let j = 0; j < item.dependsOn.length; j++) {
      const dependency = item.dependsOn[j];
      if (previousDependencies.includes(dependency)) {
        previousDependencies.push(dependency)
        return previousDependencies
      }
      const dependencyItem = this.findDependency(dependency, items)
      if (dependencyItem === undefined) throw new Error(`${item.filePath} needs ${dependency} but couldn't be found in given items.`)
      previousDependencies.push(dependency)
      const subResult = this.hasCirculairDependency(dependencyItem[1], items, previousDependencies)
      if (subResult !== null) return subResult
      else previousDependencies.pop()
    }

    return null
  }

  protected performCustomSorted(items: C[]): C[]
  {
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (item.sorter && item.customSorted !== true) {
        const nIndex = item.sorter(items, items.filter((item) => item.customSorted === true))
        if (nIndex === null) {
          item.customSorted = false
          continue
        }
        item.customSorted = true
        if (nIndex === -1) {
          continue
        } else if (i > nIndex) {
          items.splice(nIndex, 0, item)
          items.splice(i-1, 1)
        } else if (i < nIndex) {
          items.splice(i, 1)
          items.splice(nIndex-1, 0, item)
        }
      }
    }

    return items
  }

  public sort(): C[]
  {
    let items: C[] = Array.of(...this._items)

    items.sort((a, b) => a.dependsOn.length - b.dependsOn.length)

    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      const circulairDependencies = this.hasCirculairDependency(item, items)
      if (circulairDependencies !== null) {
        throw new Error(`Failed to auto sort build files, circulair dependency found: ${circulairDependencies.join(" => ")}.`)
      }
    }

    let results: C[] = []

    const lastLengts: [number, number] = [items.length, results.length]
    while (items.length !== 0) {
      const [inputItems, sortedItems] = this._sortingProcess(items, results)
      items = inputItems
      results = sortedItems
      if (lastLengts[0] === items.length && lastLengts[1] === results.length) {
        throw new Error(`Failed to auto sort build files. Dependency sorting didn't sort anything since last round. ${lastLengts[0]} <=> ${lastLengts[1]}.`)
      } else {
        lastLengts[0] = items.length
        lastLengts[1] = results.length
      }
    }

    let customFalseSortedLength: number = 0

    do {
      results = this.performCustomSorted(results)
      const nCSL = results.filter((item) => (item.customSorted === false)).length
      if (nCSL === customFalseSortedLength && nCSL !== 0) throw new Error(`Failed to further the custom sorting. Stuck at ${nCSL} items still needing to be custom sorted.`)
      customFalseSortedLength = nCSL
    } while (customFalseSortedLength > 0)

    return this._items = results
  }
}
