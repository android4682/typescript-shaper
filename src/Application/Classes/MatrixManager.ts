import { TMatrix, TMatrixColumn, TMatrixRow } from "../../Domain/Interfaces/Matrix";

export interface MatrixManagerValueRequirements
{
  length: number
}

export class MatrixManager<T extends MatrixManagerValueRequirements>
{
  private _matrix: TMatrix<T> = []

  public forEachRow(callbackFunc: (row: TMatrixRow<T>, rowIndex: number) => boolean | void): void
  {
    for (let rowIndex = 0; rowIndex < this._matrix.length; rowIndex++) {
      const row = this._matrix[rowIndex];
      if (row === undefined) continue
      if (callbackFunc(row, rowIndex) === false) break
    }
  }

  public forEachColumn(callbackFunc: (column: TMatrixColumn<T>, columnIndex: number, rowIndex: number) => boolean | void): void
  {
    for (let rowIndex = 0; rowIndex < this._matrix.length; rowIndex++) {
      const row = this._matrix[rowIndex];
      if (! row) continue
      for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
        const columnRow = row[columnIndex];
        if (columnRow === undefined) continue
        if (callbackFunc(columnRow, columnIndex, rowIndex) === false) break
      }
    }
  }

  public getRow(rowIndex: number): TMatrixRow<T>
  {
    return this._matrix[rowIndex] || []
  }

  public getNextEmptyRowIndex(): number
  {
    return this._matrix.length
  }

  public getNextEmptyRow(): TMatrixRow<T>
  {
    return this.getRowOrCreate(this.getNextEmptyRowIndex())
  }

  private getRowOrCreate(rowIndex: number): TMatrixRow<T>
  {
    if (! this._matrix[rowIndex]) this._matrix[rowIndex] = []

    return this._matrix[rowIndex]
  }

  public getColumnRow(rowIndex: number, columnIndex: number): TMatrixColumn<T> | undefined
  {
    let row = this.getRow(rowIndex)

    return row[columnIndex] || undefined
  }

  public addColumnToNextEmptyRow(columnIndex: number, value: T): this
  {
    const rowIndex = this.getNextEmptyRowIndex()

    return this.addColumnRow(rowIndex, columnIndex, value)
  }

  public addColumnRow(rowIndex: number, columnIndex: number, value: T): this
  {
    const row = this.getRowOrCreate(rowIndex)
    row[columnIndex] = value

    return this
  }

  public getMaxColumnLength(columnIndex: number): number
  {
    let maxLength = 0
    this.forEachColumn((value, _columnIndex) => {
      if (columnIndex !== _columnIndex || ! value) return true
      if (value.length > maxLength) maxLength = value.length
    })

    return maxLength
  }
}