import { StaticImplements } from "@android4682/typescript-toolkit";
import { IStaticHelpModuleManager } from "../../Domain/Interfaces/HelpModuleManager";
import { IHelpBuildArgumentModule } from "../../Domain/Interfaces/HelpModules";
import { MatrixManager } from "./MatrixManager";

export type TStaticHelpModuleManager = StaticImplements<IStaticHelpModuleManager, typeof HelpModuleManager>

interface HelpFormatterHelper
{
  value: string | string[]
}

interface HelpFormatterHelpers
{
  header: string
  spaceBetweenNextColumn: number
  maxLengthOfChild?: number
  children: HelpFormatterHelper[]
}

interface ExtraLine
{
  value: string
}

interface ExtraLines
{
  columnPosition: number
  maxChildLength: number
  columns: ExtraLine[]
}

export class HelpModuleManager implements TStaticHelpModuleManager
{
  protected static spaceBetweenColumns: number = 2

  private static getRemainingInSpaces(remaining: number, offset: number): string
  {
    let remainingSpaces = ''
    if (remaining > 0) for (let i = 0; i < remaining; i++) remainingSpaces += ' '
    for (let i = 0; i < offset; i++) remainingSpaces += ' '
    return remainingSpaces
  }

  private static getArgumentSelectorFormatted(module: IHelpBuildArgumentModule): string
  {
    return module.argumentSelector.join(', ') + ` ${module.passingArguments.join(' ')}`
  }

  private static findExtraLineColumnPosition(extraLines: ExtraLines[], columnPosition: number): boolean
  {
    if (columnPosition <= 0) return false
    for (let i = 0; i < extraLines.length; i++) {
      const extraLine = extraLines[i];
      if (extraLine.columnPosition === columnPosition) return true
    }

    return false
  }

  private static addHelpModuleValueToMatrixManager(input: string[] | string, manager: MatrixManager<any>, rowIndex: number, columnIndex: number): typeof manager
  {
    if (Array.isArray(input)) {
      if (input.length > 1) input.push('')
      for (let i = 0; i < input.length; i++) {
        const value = input[i];
        manager.addColumnRow(rowIndex + i, columnIndex, value)
      }
    } else manager.addColumnRow(rowIndex, columnIndex, input)
    
    return manager
  }

  private static formatSpacing(matrixManager: MatrixManager<string>): string
  {
    let formattedText: string = ''
    matrixManager.forEachRow((row, rowIndex) => {
      let line: string = ''
      for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
        const columnValue = row[columnIndex];
        if (! columnValue) line += this.getRemainingInSpaces(matrixManager.getMaxColumnLength(columnIndex), this.spaceBetweenColumns)
        else line += `${columnValue}${this.getRemainingInSpaces(matrixManager.getMaxColumnLength(columnIndex) - columnValue.length, this.spaceBetweenColumns)}`
      }
      formattedText += line.trimEnd()
      formattedText += '\n'
    })

    return formattedText
  }

  public static formatModules(modules: IHelpBuildArgumentModule[], target: 'GLOBAL' | 'BUILDER' = 'BUILDER'): string
  {
    if (modules.length === 0) return ''
    
    let maxArgumentSelectorLength = 0
    for (let i = 0; i < modules.length; i++) {
      const helpModule = modules[i];
      const formattedArgumentSelector = this.getArgumentSelectorFormatted(helpModule)
      if (formattedArgumentSelector.length > maxArgumentSelectorLength) maxArgumentSelectorLength = formattedArgumentSelector.length
    }
    maxArgumentSelectorLength += this.spaceBetweenColumns
    
    const headers: string[] = [
      'Arguments',
      'Default',
      'Description'
    ]
    
    if (target === 'BUILDER') {
      headers.push('Recommanded by')
      headers.push('Used by')
    }

    let helpOutputManager = new MatrixManager<string>()

    for (let i = 0; i < headers.length; i++) {
      const header = headers[i];
      helpOutputManager.addColumnRow(0, i, header)
      let styling = ''
      for (let j = 0; j < header.length; j++) styling += '-'
      helpOutputManager.addColumnRow(1, i, styling)
    }
    
    for (let i = 0; i < modules.length; i++) {
      const helpModule = modules[i];
      const rowIndex = helpOutputManager.getNextEmptyRowIndex()
      this.addHelpModuleValueToMatrixManager(this.getArgumentSelectorFormatted(helpModule), helpOutputManager, rowIndex, 0)
      this.addHelpModuleValueToMatrixManager(helpModule.defaultValue, helpOutputManager, rowIndex, 1)
      this.addHelpModuleValueToMatrixManager(helpModule.description, helpOutputManager, rowIndex, 2)
      if (target === 'BUILDER') {
        helpModule.requiredBy.sort()
        this.addHelpModuleValueToMatrixManager(helpModule.requiredBy, helpOutputManager, rowIndex, 3)
        helpModule.usedBy.sort()
        this.addHelpModuleValueToMatrixManager(helpModule.usedBy, helpOutputManager, rowIndex, 4)
      }
    }
    
    return this.formatSpacing(helpOutputManager)
  }
}
