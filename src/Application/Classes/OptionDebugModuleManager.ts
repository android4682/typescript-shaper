import { StaticImplements } from "@android4682/typescript-toolkit";
import { IOptionDebugModule } from "../../Domain/Interfaces/OptionDebugModule";
import { IStaticOptionDebugModuleManager } from "../../Domain/Interfaces/OptionDebugModuleManager";

export type TStaticOptionDebugModuleManager = StaticImplements<IStaticOptionDebugModuleManager, typeof OptionDebugModuleManager>

export class OptionDebugModuleManager implements TStaticOptionDebugModuleManager
{
  protected static spaceBetweenColumns: number = 1
  
  private static getRemainingInSpaces(remaining: number): string
  {
    let remainingSpaces = ''
    for (let i = 0; i < remaining; i++) remainingSpaces += ' '
    return remainingSpaces
  }
  
  public static formatModules(modules: IOptionDebugModule[]): string
  {
    if (modules.length === 0) return ''

    let maxArgumentSelectorLength = 0
    for (let i = 0; i < modules.length; i++) {
      const optionModule = modules[i];
      if (optionModule.key.length > maxArgumentSelectorLength) maxArgumentSelectorLength = optionModule.key.length
    }
    maxArgumentSelectorLength += this.spaceBetweenColumns
    
    let formattedOptionDebugText = ''
    for (let i = 0; i < modules.length; i++) {
      const optionModule = modules[i];
      const value = (typeof optionModule.value === 'string') ? `'${optionModule.value}'` : optionModule.value
      formattedOptionDebugText += `${optionModule.key}${this.getRemainingInSpaces(maxArgumentSelectorLength - optionModule.key.length)}= ${value}\n`
    }

    return formattedOptionDebugText
  }
}