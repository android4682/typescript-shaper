import { EBuildProcessResultTypes, IBuildProcessResult, IStaticBuildProcess, TBuildProcessResults } from "../../Domain/Interfaces/BuildProcess";
import { IBuildStepManager, TBuildStep } from "../../Domain/Interfaces/BuildStepManager";

export class BuildStepManager implements IBuildStepManager
{
  protected _steps: TBuildStep[]

  public constructor(steps: TBuildStep[] = [])
  {
    this._steps = steps
  }

  public forEach(callbackfn: (buildProcess: IStaticBuildProcess, type: EBuildProcessResultTypes, stepNumber: number) => boolean | void): void
  {
    for (let i = 0; i < this._steps.length; i++) {
      const buildProcessStep = this._steps[i];
      if (callbackfn(buildProcessStep, buildProcessStep.returnType, i) === false) break
    }
  }

  public async forEachAsync(callbackfn: (buildProcess: IStaticBuildProcess, type: EBuildProcessResultTypes, stepNumber: number) => Promise<boolean | void>): Promise<void>
  {
    for (let i = 0; i < this._steps.length; i++) {
      const buildProcessStep = this._steps[i];
      if (await callbackfn(buildProcessStep, buildProcessStep.returnType, i) === false) break
    }
  }

  public static getAnyValidStepResult(allStepResults: TBuildProcessResults, validStepNames: string[], fallbackType: EBuildProcessResultTypes | undefined = undefined): IBuildProcessResult<any>
  {
    for (let i = 0; i < validStepNames.length; i++) {
      const stepName = validStepNames[i];
      if (allStepResults.has(stepName)) return <IBuildProcessResult<any>> allStepResults.get(stepName)
    }
    
    let result: any = undefined
    if (fallbackType) {
      allStepResults.forEach((value, name) => {
        if (value.type === fallbackType) {
          result = value
          return false
        }
      })
    }

    if (! result) throw new Error('Step not found.')
    else return result
  }

  public getAllSteps(): TBuildStep[]
  {
    return this._steps
  }

  public overrideAllSteps(steps: TBuildStep[]): this
  {
    this._steps = steps

    return this
  }

  public getStep(step: number): TBuildStep
  {
    let result = this._steps[step]
    if (! result) throw new Error('Out of bounds.')
    return result
  }

  public getStepByName(stepName: string): TBuildStep
  {
    for (let i = 0; i < this._steps.length; i++) {
      const buildProcessStep = this._steps[i];
      if (buildProcessStep.name === stepName) return buildProcessStep
    }

    throw new Error('Step not found.')
  }
}